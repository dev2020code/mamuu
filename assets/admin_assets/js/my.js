
/*var csrf=document.getElementById('csrf_token').name;
var hash=document.getElementById('csrf_token').value;*/


var csrf=$("#csrf_token").attr("name");
var hash=$("#csrf_token").val();
var add='&'+csrf +'='+hash;

var xhr=null;
function state_change(e,city_id,fun)
{
	
	if(xhr!=null)
	{
		xhr.abort();
	}
	var state_id=e.value;
	$('#'+city_id).addClass('loadinggif');
	xhr= $.ajax(
     {
        type: 'post',
	    url: site_url+'supplier/get_city',
	    dataType: 'json',
	    data: 'id=' + state_id+add,
	    success: function(html) 
	    {
	        if (html.status == '1') 
	        {
	            var txt = "<option value=''>Select </option>";
	            $.each(html.message, function(k, v) {
	                txt += "<option value='" + v['city_id'] + "'>" + v['city_name'] + "</option>";
	            });
	            
	             $('#' + city_id).html(txt);
	             /*window[fun]();*/
	             
	            
	        } 
	        else 
	        {
	           $('#' + city_id).html('<option value="">-- None --</option>');
	        }
	    },
	   	complete: function(html) 
	   	{
			$('#'+city_id).removeClass('loadinggif');
	    }
	    
	    
	});
	
}

function country_change(e,state_id,city_id,fun)
{
	if(xhr!=null)
	{
		xhr.abort();
	}
	
	var country_id=e.value;
	
	$('#'+city_id).addClass('loadinggif');
	$('#'+state_id).addClass('loadinggif');
	  xhr = $.ajax(
     {
        type: 'post',
	    url: site_url+'fetch/get_state',
	    dataType: 'json',
	    data: 'id=' + country_id +add,
	    success: function(html) 
	    {
	        if (html.status == '1') 
	        {
	            var txt = "<option value=''>Select </option>";
	            $.each(html.message, function(k, v) {
	                txt += "<option value='" + v['state_id'] + "'>" + v['state_name'] + "</option>";
	            });
	            
	             $('#' + state_id).html(txt);
	             $('#' + city_id).html('<option value="">-- None --</option>');
	            
	        } 
	        else 
	        {
	           $('#' + state_id).html('<option value="">-- None --</option>');
	           $('#' + city_id).html('<option value="">-- None --</option>');
	        }
	    },
	   	complete: function(html) 
	   	{
			$('#'+city_id).removeClass('loadinggif');
			$('#'+state_id).removeClass('loadinggif');
	    }	    
	});
}

$('#submit').on('submit',function(){
	if(xhr!=null)
	{
		xhr.abort();
	}	
	var submit_url=$(this).attr('action');
	var call_function=$(this).attr('call-function');
	var call_method=$(this).attr('method');
	var call_type=$(this).attr('call-type');
	var x=0;
	var err='';
	$('#loader').show();
	$("#submit [name='submit']").attr('disabled',true);
	/*return false;		*/
	  xhr = $.ajax(
	  {
		type: 'POST',
		url: site_url+submit_url,
		dataType: "json",
		data:$( "#submit" ).serialize() ,
		success: function(html)
		{	
			
			if(html.status==0)
			{
				// other Errors
				if(html.errortype==1)
				{
					err=html.message;
				}
				else
				{
					// Validation Errors
					$('.form-control').removeClass('error');
					var s=0;
					//split errors and append to Page top
					$.each(html.message, function(k,v) 
					{
						err+='<p><i class="fa fa-ban"></i> '+v+'</p>';					
						if(s<1)
						{
							$("#submit [name='"+k+"']").focus();
							s++;
						}					
			   			$("#submit [name='"+k+"']").addClass( "error" );
					}); 
				}
				$('#error_add').html('<div id="msg" class="alert alert-danger alert-dismissable">'+err+'</div>');
			}
			else
			{
					$('#result').html(html.message);
					$('#myModal').modal('show');
					$('#myModal').on('hidden.bs.modal', function () 					{
					location.reload();
					});
			}
				
		},
		complete: function (data) 
		{
			$('#loader').hide();
			$("#submit [name='submit']").attr('disabled',false);	
		},
		 error: function(jqXHR, textStatus, errorThrown) { alert('Please try again'); }
				
	 });
	return false;
});