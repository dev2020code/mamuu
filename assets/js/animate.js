/*youtube video*/

 $(window).on('resize load', function() {
  $('.site-header iframe').each(function() {
    var self = $(this);
    var container = self.parent();

   self.css({
      width: container.width() + "px",
      height: container.width() * (9/16) + 'px',
      position: 'absolute',
      marginTop: -container.width() * (9/32) + 'px',
      top: '50%'
    });
  });
});
// Load the IFrame Player API code asynchronously.
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
  function onYouTubePlayerAPIReady() {
    player = new YT.Player('yt-player', {
      autoplay : 1,
      videoId: 'm4ICyR3QTR0',
      playerVars : {
        'autoplay' : 1,
        'rel' : 0,
        'showinfo' : 0,
        'showsearch' : 0,
        'controls' : 0,
        'loop' : 1,
        'enablejsapi' : 1,
        'playlist': 'm4ICyR3QTR0'
      },
      events: {
      	"onReady": onPlayerReady,
      	//"onStateChange": onPlayerStateChange
 			}
	});
}



/*Scroll Animation*/

jQuery(function($) {
  
  var doAnimations = function() {
    var offset = $(window).scrollTop() + $(window).height(),
        $animatables = $('.animatable');
    if ($animatables.length == 0) {
      $(window).off('scroll', doAnimations);
    }
		$animatables.each(function(i) {
       var $animatable = $(this);
			if (($animatable.offset().top + $animatable.height() - 20) < offset) {
        $animatable.removeClass('animatable').addClass('animated');
			}
    });

	};
	$(window).on('scroll', doAnimations);
  $(window).trigger('scroll');

});