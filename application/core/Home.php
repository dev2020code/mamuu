<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	 public function __construct()
	 {
	 	parent::__construct();
	 }
	 public function index()
	 {

		$this->load->view('master/home-view');

	 }
	 public function Employee()
	 {
	 	
		$this->load->view('master/EmployeeRegister-view');

	 }
	 public function Employer()
	 {
	 	
		$this->load->view('master/EmployerRegister-view');

	 }
	 public function Gallery()
	 {
	 	
		$this->load->view('master/Gallery-view');

	 }
	 public function add_employee_process()
	 {
	   if ($this->input->is_ajax_request())
	   {
			$data=$this->input->post();
			// var_dump($_FILES['empFile']['name']);
			// die;
			$this->load->library('form_validation');
			$this->form_validation->set_rules('empName','Employee Name','required');			
			$this->form_validation->set_rules('empDegree','Employee Degree','required');
			$this->form_validation->set_rules('empCoName','College name/ Company name','required');
			$this->form_validation->set_rules('empPhNumber','Employee Phone Number','required|numeric|max_length[15]');
			$this->form_validation->set_rules('empAltPhNumber','Employee Alternative Number','required|numeric|max_length[15]');
			$this->form_validation->set_rules('empEmail','Employee Email','required|valid_email');
			$this->form_validation->set_rules('empFname','Employee Full Name','required');
			$this->form_validation->set_rules('empAdd1','Employee Address1','required');
			$this->form_validation->set_rules('empAdd2','Employee Address2','required');
			$this->form_validation->set_rules('empState','Employee State','required');
			$this->form_validation->set_rules('empCity','Employee City','required');
			$this->form_validation->set_rules('empPin','Employee Pincode','required|numeric');
			if(empty($_FILES['empFile']['name'])) {
			$this->form_validation->set_rules('empFile','Employee Resume','required');
			}
			// $this->form_validation->set_rules('dom_provider','Domain Provider','required|numeric');
		    if($this->form_validation->run()!==false)
		   	{
		   		
			    	$file_data = $this->upload_file('empFile');
			        if (isset($file_data['success'])) {
			            $attach = $file_data['success']['full_path'];
			        } else {
			        	$message['empFile'] = $this->upload->display_errors();
			             $report  = array('status' => 0,'message' => $message);
					     echo json_encode($report);
					     exit;
			        }
			        $data['empFile']=$file_data['success']['file_name'];
				$emp_id=$this->common_model->insert_table('employee',$data);
			    if($emp_id!==false)
			    {
			        $subject = 'Employee Registration';
                	$text    = 'Name :'.$data['empName'].'<br> Degree :'.$data['empDegree'].'<br> Collegename/ company name :'.$data['empCoName'].'<br> Phone Number :'.$data['empPhNumber'].'<br> Alternative Phone Number :'.$data['empAltPhNumber'].'<br> Email :'.$data['empEmail'].'<br> Full Name: '.$data['empFname'].'<br> Address Line 1 :'.$data['empAdd1'].'<br>  Address Line 2 :'.$data['empAdd2'].'<br> State :'.$data['empState'].'<br> City :'.$data['empCity'].'<br>  Pin Code :'.$data['empPin'];
                	$to 	= 'phoenixhrplacement@gmail.com';
		                $this->load->library('email_lib');
		                $this->email_lib->normal_mail($to, $subject, $text, $attach);

			    	$message='Employee Registration Successfully';
			        $report = array('status' => 1,'message' => $message);
			        echo json_encode($report);
					exit;
			    }
			    else
			    {
			        $message = 'Something wrong please try again';
			        $report  = array('status' => 0,'message' => $message);
			        echo json_encode($report);
			        exit;

			    }
		   	}
		   	else
		   	{
		     $message = $this->form_validation->error_array();
		     $report  = array('status' => 0,'message' => $message);
		     echo json_encode($report);
		     exit;
		    }
	    }
	    else
	   {
	     show_error("No direct access allowed");
	     //or redirect to wherever you would like
	   }
	}
	public function add_employer_process()
	 {
	   if ($this->input->is_ajax_request())
	   {
			$data=$this->input->post();
			$this->load->library('form_validation');
			$this->form_validation->set_rules('emplrComName','Employer Company','required');			
			$this->form_validation->set_rules('emplrPhNumber','Employer Phone Number','required|numeric|max_length[15]');
			$this->form_validation->set_rules('emplrWaNumber','Employer Whatsapp Number','required|numeric|max_length[15]');
			$this->form_validation->set_rules('emplrEmail','Employer Email','required|valid_email');
			$this->form_validation->set_rules('emplrPosition','Name of Vacant Position','required');
			$this->form_validation->set_rules('emplrVacNo','No of Vacant Position','required|numeric');
			$this->form_validation->set_rules('emplrSalary','Salary','required');
			$this->form_validation->set_rules('emplrEmail','Email','required|valid_email');
			if(empty($_FILES['emplrFile']['name'])) {
				$this->form_validation->set_rules('emplrFile','File','required');
			}
		    if($this->form_validation->run()!==false)
		   	{

			    	$file_data = $this->upload_file('emplrFile');
			        if (isset($file_data['success'])) {
			            $attach = $file_data['success']['full_path'];
			        } else {
			        	$message['empFile'] = $this->upload->display_errors();
			             $report  = array('status' => 0,'message' => $message);
					     echo json_encode($report);
					     exit;
			        }
			        $data['emplrFile']=$file_data['success']['file_name'];
			        
		   		 // $data['emplrFile']=$file_data['file_name'];
		   		
				$emp_id=$this->common_model->insert_table('employer',$data);
			    if($emp_id!==false)
			    {
			         $subject = 'Employer Registration';
                	$text    = 'Company Name :'.$data['emplrComName'].'<br> Phone Number :'.$data['emplrPhNumber'].'<br> Whatsapp Number :'.$data['emplrWaNumber'].'<br> Email :'.$data['emplrEmail'].'<br> Name of the Vacant Position :'.$data['emplrPosition'].'<br> No.of Vancies :'.$data['emplrVacNo'].'<br> Salary: '.$data['emplrSalary'].'<br> Company Profile :'.$data['emplrFile'];
	        		$to 	= 'phoenixhrplacement@gmail.com';
	                $this->load->library('email_lib');
	                $this->email_lib->normal_mail($to, $subject, $text, $attach);

			    	$message='Employer Registration Successfully';
			        $report = array('status' => 1,'message' => $message);
			        echo json_encode($report);
					exit;
			    }
			    else
			    {
			        $message = 'Something wrong please try again';
			        $report  = array('status' => 0,'message' => $message);
			        echo json_encode($report);
			        exit;

			    }
		   	}
		   	else
		   	{
		     $message = $this->form_validation->error_array();
		     $report  = array('status' => 0,'message' => $message);
		     echo json_encode($report);
		     exit;
		    }
	    }
	    else
	   {
	     show_error("No direct access allowed");
	     //or redirect to wherever you would like
	   }
	}
	function upload_file($filename='')
    {
    	
        // $config['upload_path']   = $_SERVER['DOCUMENT_ROOT'];
        $config['allowed_types'] = 'doc|docx|pdf';
         $new_name                = time();
        $config['file_name']     = $new_name . '_' . 'resume';
        $config['upload_path']   = site_path.'assets/phoenix';
        $config['max_size']      = 10000;
        if ($filename=='emplrFile') {
        	$config['file_name']     = $new_name . '_' . 'Company Profile';
        }

        $this->load->library('upload', $config);
     //    var_dump($filename);
    	// die;
        if ($this->upload->do_upload($filename)) {
            return array('success'=>$this->upload->data());
        } else {
            return array('failure'=>$this->upload->display_errors());
        }
    }
 }
 ?>
