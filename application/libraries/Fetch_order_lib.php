<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Fetch_order_lib
{
	 private $byzero;
	 function __construct()
	 {
		$this->byzero = & get_instance();
	 }
	 
	 public function fetch_user_order_list($user_id=0,$supp_id=0)
	 {	 	

	 	if(!empty($user_id))
		{
			$order_detail = $this->byzero->common_model->__fetch_contents('booking_master' , array('order_master_user_id' => $user_id , 'order_master_removed' => '0'));
		}
		
		if(!empty($supp_id))
		{
			$order_detail = $this->byzero->common_model->__fetch_contents('booking_master' , array('order_master_supp_id' => $supp_id , 'order_master_removed' => '0'));
		}
		
		
		return $order_detail;
		
	 }
	 
	  public function fetch_user_order_list_items($ord_id=0)
	 {	 	
	 
	 	if(!empty($ord_id))
		{
			$order_list_detail = $this->byzero->common_model->__fetch_contents('booking_item_table' , array('booking_item_booking_master_no' => $ord_id , 'booking_item_removed' => '0'));
		}
		
		
		return $order_list_detail;
		
	 }
	
	
	




}