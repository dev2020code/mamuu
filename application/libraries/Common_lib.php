<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Common_lib
{
   private $byzero;
   /**
   *  this is common library this autoincluded to all the values
   *
   * @return
   */

   public function __construct()
   {
	 	$this->byzero = & get_instance();
		$this->byzero->load->model(array('common_model'));
		date_default_timezone_set('Asia/Kolkata');
		$this->login_validate();
   }


   public function login_not_using_pages()
   {
   	$array=array('login','complaints');
   	return $array;
   }

  /**
  *  login validate function
  *  this method works every page loads and it wllvalidate the every page realoding
  *
  * @return
  */
   public function login_validate()
   {
   	  if(!isset($this->byzero->session->userdata[user_id]))
   	  {
   	  	$varx=explode('/',uri_string());

   	  	if(in_array($varx[0],$this->login_not_using_pages()))
   	  	{
   	  		//redirect('login', 'refresh');

		}
		else
		{

			$this->byzero->session->set_userdata('url_login',uri_string());
			redirect(site_url.'login', 'refresh');

		}


	  }
	  else
	  {
	 // 	var_dump($this->byzero->router->fetch_class());

	  	if(uri_string()=='login')
	  	{
	  		$this->update_session();
			redirect(site_url.'', 'refresh');
		}
		else
		{
			$result=$this->check_permission();
			if($result==FALSE)
			{
				redirect(site_url.'', 'refresh');
			}
			else
			{
				$this->update_session();
			}

		}

	  }
   }

   /**
   * update session will check that current session and change it into
   * current time and validtill
   *
   * @return true or redirect to login page
   */
   public function update_session()
   {

   		$this->byzero->session->set_userdata('last_activity',time());
   	$log_details=$this->byzero->common_model->fetch_contents('log',array('log_id'=>$this->byzero->session->userdata[user_id]));
   $valid_till=$log_details[0]['log_valid_till']+valid_till;
    if($valid_till>=time())
   {
   	$id=$this->byzero->session->userdata(user_id);
   	$data=array('log_valid_till'=>$valid_till,'log_time_of_sec'=>date('Y-m-d H:i:s'),'log_last_accessed'=>time());
   	 $this->byzero->common_model->update_table('log',$data,$id);

   }
   else
   {
   	  $this->byzero->session->unset_userdata(user_id);
   	  $message="Your session expired";
			 $report=array('status'=>0,'form_data'=>array('user_name'=>$this->byzero->session->userdata['user']['user_name']),'message'=>$message);
			$this->byzero->session->unset_userdata('user');
			$this->byzero->session->set_userdata('url_login',uri_string());
			$this->byzero->session->set_userdata('form',array('login'=>$report));
			redirect(site_url.'login','refresh');
   }
   }


   /**
   *
   * @param undefined $id_value
   * @param undefined $value
   * @param undefined $array
   *
   * @return
   */
   public function options($id_value,$value,$array)
   {
   	$string="<option value=''>Select</option>";
   	$none="<option value=''>None</option>";
   	if(empty($array))
   	{
		return $none;
	}
	else
	{
		foreach($array as $item)
   	{
		$string.="<option value='".$item[$id_value]."'>".$item[$value]."</option>";
	}
	  return $string;
	}

   }

   /**
   *
   * @param int $id
   * @param string $value
   * @param array $array
   * @param  int $value
   *
   * @return string contains  filloption
   */
   public function options_select($id_value,$value,$array,$match_id)
   {
   	$string="<option value=''>Select</option>";
   	$none="<option value=''>None</option>";
   	if(empty($array))
   	{
		return $none;
	}
	else
	{
		foreach($array as $item)
   	{
   		if($item[$id_value]==$match_id)
   		{
			$string.="<option selected='selected' value='".$item[$id_value]."'>".$item[$value]."</option>";
		}
		else
		{
			$string.="<option  value='".$item[$id_value]."'>".$item[$value]."</option>";
		}

	}
	  return $string;
	}

   }


   /**
   *
   * @param int $id
   * @param string $value
   * @param array $array
   * @param  int $value
   *
   * @return string contains  filloption
   */
   public function verify_email($email_id,$mobile_no,$user_id)
   {
	   $email_random=random_string('unique');
	   $mobile_random=rand(0001,9999);
	   ///sms
	   $username= urlencode('Byport');
	   $password= urlencode('byport');
	   $admin_phone=urlencode($mobile_no);


   $data=array(
  		'user_id'=>$user_id,
		'user_verify_email'	=>$email_random,
		'user_verify_mobile'=>$mobile_random
  );
   $insert_verify_email_details=$this->byzero->common_model->insert_table('user_verify',$data);
  if($insert_verify_email_details == true)
  {



 // echo base_url("recruiter/verify").'?user_id='.$user_id.'&'.'verify_key='.$email_random;
   		 $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';
		$this->byzero->load->library('email');
        $this->byzero->email->initialize($config);

        $this->byzero->email->from('jobs@jobexpress.in', 'Jobexpress ');
        $this->byzero->email->to($email_id);

        $this->byzero->email->subject('Verify Email from job express');
        $html = 'Dear User,<br>Please click on below URL or paste into your browser to verify your Email Address<br><a href="'.site_url.'recruiter/verify/user_id='.$user_id.'&'.'verify_key='.$email_random.'">click here</a> <br> copy this url '.site_url.'recruiter/verify/user_id='.$user_id.'&'.'verify_key='.$email_random.' <br><br>Thanks<br><br>Admin Team<br><a href="'.site_url.'">Job Express</a>';
        $this->byzero->email->message($html);

        $this->byzero->email->send();


	  /*$this->byzero->email->from('info@byzerotechnologies.com', 'Ram ');
	  $this->byzero->email->to($email_id);
 	  $this->byzero->email->subject('Email Test');
	  $this->byzero->email->message('Dear User,<br>Please click on below URL or paste into your browser to verify your Email Address\n\n'.site_url.'recruiter/verify/user_id='.$user_id.'/'.'verify_key='.$email_random.' <br>'.'<br><br>Thanks<br>Admin Team');
	  $this->byzero->email->send();*/








		$phone_message="your Verification Code is".$mobile_random;
		$msg1=urlencode($phone_message);
		$msg_url = 'http://sms.sakthiinfotech.com/api/sendmsg.php?user='.$username.'&pass='.$password.'&sender=BYPORT&phone='.$admin_phone.'&text='.$msg1.'&priority=ndnd&stype=normal';

		$ch = curl_init($msg_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$curl_scraped_page = curl_exec($ch);
		curl_close($ch);


    }
    }

  	/**
	  *
	  *
	  * @return
	  */
  	public function check_permission()
  	{

		$url=$this->byzero->router->fetch_class();
		//var_dump($url);
		$user=$this->byzero->session->userdata('user');
		if($user['user_group']==1)
		{
			return TRUE;
		}
		else
		{
			$allowed=$this->byzero->common_model->permission_check($url);
			if($allowed!=FALSE)
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}

		}
	}


}
