<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Validate_login
{
	private $byzero;
	function __construct()
	{
		$this->byzero= & get_instance();
		$this->byzero->load->model(array('login_model','common_model'));
		$this->validate_login();
		$this->is_logged();
	}
	public function is_logged()
     {
		if(!empty($this->byzero->session->userdata['user_data']) )
		{
		   return TRUE;
		}
		else
		{

			//$this->byzero->load->library(array('facebook_lib','google_lib'));
		    return FALSE;
		}
     }
	public function validate_login()
	{
		$class_name=$this->byzero->router->fetch_class();
		if($this->is_admin()==TRUE)
		{
			$this->byzero->load->library(array('users_lib'));
			$this->byzero->load->model(array('user_model'));
		}
		// @parma $login_exceptions Exception Controllers list to skip the Session check (Validate Login) process
		$login_exceptions=array('login','home','complaints','committee_members');
		if(!in_array($class_name,$login_exceptions))
		{
			$sesson_check=$this->check_session();
			//var_dump($sesson_check);
			if($sesson_check['0']==false)
			{
				if($sesson_check['1']==false)
				{
					$this->logout();
					return FALSE;
				}
			}
			else
			{
				//check the page paermission, if the page is a admin page. Frontend pages(Normal User pages) will not be checked
				if($this->is_admin()==TRUE)
				{
					//$check_page_permission=$this->check_page_permission();
					return TRUE;
				}
				else
				{
					$check_page_permission=TRUE;
				}
				if(($sesson_check['1']==TRUE)&&($check_page_permission==TRUE))
				{
					// Valid session and updated
					return true;
				}
				else
				{
					$this->logout();
					return false;
				}

			}
		}
		else
		{
			return true;
		}
	}
	public function check_page_permission()
	{
		$group_id=$this->byzero->session->userdata['user_data']->admin_user_group;
		if(!empty($group_id))
		{
			/**
			*
			* @group_id = 1 -> Super Admin
			* @group_id = 2 -> Admin
			* @group_id = 3 -> Manager
			* @group_id = 4 -> Designer
			* @group_id = 5 -> Customer
			*
			*/
			if(($group_id=='1')||($group_id=='2'))
			{
				// admins have access to all the pages.
				return TRUE;
			}
			else
			{
				$this->byzero->load->library('common_lib');
				$page_name=$this->byzero->router->fetch_class();
				$constrains=array(array('type'=>'where','field_name'=>'usergroup_id','value'=>$group_id));
				$group_name_alias=$this->byzero->common_model->general_select('usergroup',$constrains,'*');
				$group_name_alias=$group_name_alias['0']->user_group_name_alias.'_menu';
				$user_pages=$this->byzero->common_lib->$group_name_alias(TRUE);
				$page_details=$this->byzero->common_model->get_page_details($page_name);
				if(!empty($page_details))
				{
					$page_id=$page_details['0']->page_id;
					//return true if the page is not listed in menu
					if(in_array($page_details['0']->page_name,$this->pages_not_in_menu()))
					{
						return true;
					}
					//echo '<pre>'.print_r($user_pages,1).'</pre>';
					foreach($user_pages['main_menus'] as $main_menu)
					{
						if($main_menu['3']==$page_id)
						{
							return TRUE;
						}
					}
					foreach($user_pages['sub_menus'] as $sub_menu)
					{
						foreach($sub_menu as $menu)
						{
							if($menu['2']==$page_id)
							{
								return TRUE;
							}
						}
					}
					return FALSE;
				}
				else
				{
					return FALSE;
				}
			}
			/*else
			{
				return FALSE;
			}*/
		}
		else
		{
			return FALSE;
		}
	}
	public function pages_not_in_menu()
	{
		return array('cart_ops');
	}
	public function process_login($email='',$password='')
	{
		$login_data=$this->byzero->login_model->process_login($email,$password);
		if($login_data!=FALSE)
		{
			if($login_data['0']->user_blocked=='1')
			{
				$login_data=FALSE;
				$this->logout('0');
				return array('status'=>FALSE,'message'=>array('You have been blocked. Please Contact Administrator!!'),'response'=>'validation_fail');
			}
			if($login_data['0']->user_verified=='0')
			{
				$login_data=FALSE;
				$this->logout('0');
				return array('status'=>FALSE,'message'=>array('Please Verify your Account!!'),'response'=>'validation_fail');
			}
			foreach($login_data['0'] as $key=>$data)
			{
				if($key=='user_password')
				{
					unset($login_data['0']->$key);
				}
				if(($key=='user_fname')||($key=='user_lname'))
				{
					$login_data['0']->$key=ucfirst($data);
				}
			}
			return array('status'=>TRUE,'message'=>'Login Succesfull','response'=>'success');
		}
		else
		{
			$this->logout('0');
			return array('status'=>FALSE,'message'=>array('Invalid Email or Password'),'response'=>'validation_fail');
		}
	}
	public function process_admin_login($username='',$password='')
	{
		$login_data=$this->byzero->login_model->process_admin_login($username,$password);
		if($login_data!=FALSE)
		{
			$cropped_login_data=$this->user_det_to_session($login_data,TRUE);
			foreach($login_data['0'] as $key=>$data)
			{
				if($key=='user_password')
				{
					unset($login_data['0']->$key);
				}
				if(($key=='user_fname')||($key=='user_lname'))
				{
					$login_data['0']->$key=ucfirst($data);
				}
			}
			return array('status'=>TRUE,'message'=>'Login Succesfull','response'=>'success','redirect'=>admin_url.'dashboard');
		}
		else
		{
			$this->logout('0');
			return array('status'=>FALSE,'message'=>array('Invalid Username or Password'),'response'=>'validation_fail');
		}
	}
	public function user_det_to_session($user_data,$update_session=FALSE)
	{
		if(!empty($user_data)&&($user_data!=FALSE))
		{
			foreach($user_data['0'] as $key=>$data)
			{
				if($key=='user_password')
				{
					unset($user_data['0']->$key);
				}
				if(($key=='user_fname')||($key=='user_lname'))
				{
					$user_data['0']->$key=ucfirst($data);
				}
			}
			if(isset($user_data['0']->user_id))
			{
				$user_profile=$this->byzero->users_lib->get_profile_photo($user_data['0']->user_id);
				$user_data['0']->user_profile_photo=$user_profile;
			}
			else
			{
				//$user_profile=$this->byzero->users_lib->get_profile_photo($user_data['0']->admin_user_id);
				$user_profile='';
			}
			if($update_session!=FALSE)
			{
				//var_dump($this->byzero->session->userdata);
				$session_data=$this->byzero->session->userdata;
				$session_data['user_data']=$user_data['0'];
				$this->byzero->session->set_userdata($session_data);
			}
			return $user_data;
		}
		else
		{
			return FALSE;
		}
	}
	public function insert_session_to_db($session_data)
	{
		return $this->byzero->login_model->insert_session_to_db($session_data);
		//return TRUE;
	}
	public function update_to_logged_session($user_data)
	{
		// update the user data to session
		$ip_address=$this->byzero->session->userdata('ip_address');
		$start_time=$last_activity=time();
		$data=array('ip_address'=>$ip_address,'start_time'=>$start_time,'last_activity'=>$last_activity);
		$this->byzero->session->set_userdata($data);
	}
	public function check_session()
	{
		if($this->is_logged()!=false)
		{
			$session_data=$this->byzero->session->userdata('user_data');
			// check the session is present  in the db and session is not expired
			$session_expire_time=$this->byzero->config->item('sess_expiration');
			$last_activity=$this->byzero->session->userdata('__ci_last_regenerate');
			//var_dump($last_activity);die;
			if(($last_activity+$session_expire_time)<=time())
			{
				//Session Expired
				return array(FALSE,FALSE);
			}
			else
			{
				//Update the Live session
				$update_session=$this->update_session();
				if($update_session==true)
				{
					return array(TRUE,TRUE);
				}
				else
				{
					return array(TRUE,FALSE);
				}
			}
		}
		else
		{
			return array(FALSE,FALSE);
		}
	}
	public function update_session()
	{
		/* unsupported content for the codeigniter 3 version */
		$last_activity=$this->byzero->session->userdata('__ci_last_regenerate');
		$this->byzero->session->set_userdata('last_activity',time());
		/*$session_data=array('last_activity'=>now());
		$update_session_data=$this->byzero->login_model->update_session_data_db($session_data);
		if($update_session_data!=false)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}*/

		return TRUE;
	}
	public function is_admin()
	{
		$root_folder=$this->byzero->uri->segment(1);
		if($root_folder==admin_folder)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function invalid_session()
	{

	}
	public function logout($redirect='1',$destroy=FALSE)
	{
		if($destroy==TRUE)
		{
			$this->byzero->session->sess_destroy();
		}
		else
		{
			$this->byzero->session->unset_userdata('user_data',array());
		}
		if($redirect=='1')
		{
			redirect(site_url.'home','refresh');
		}
	}
	public function admin_logout()
	{
		$this->byzero->session->sess_destroy();
		redirect(admin_url,'refresh');
	}
}
