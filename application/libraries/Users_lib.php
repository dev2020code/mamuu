<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users_lib 
{
   private $byzero;
   function __construct()
   {
	 $this->byzero = & get_instance();
	 $this->byzero->load->library(array('send_email','email'));
   }
   public function register_user($f_name,$l_name,$email,$password)
   {
	   $activation_key=$this->generate_link($email);
	   $activation_link=$this->generate_activation_link($activation_key,1);
	   $this->byzero->load->library('user_agent');
	   $referrer=$this->byzero->agent->referrer();
	   $inser_user=$this->byzero->user_model->register_user($f_name,$l_name,$email,$password,$activation_key,$referrer);
	   if(!empty($inser_user))
	   {
	   	   $wallet_data=array('wallet_amount'=>0.00,'wallet_user_id'=>$inser_user);
	       $insert_user_wallet=$this->byzero->common_model->general_insert('wallet',$wallet_data);
		   $subject="PIX 91 :: Registration Succesful";
		   $message="<html><head></head><body><img src='".assets_url."images/logo.png' width='150' /><br/><p>Hi ".$f_name."</p><p>Thanks for signing up!</p> You have been successfully registered with Pix91. Proceed on to explore our features! . <br/> ".anchor($activation_link,' Click here')." to activate your account for placing an order. </body></html>";
		   $send_mail=$this->byzero->send_email->send_emails($email,$subject,$message);
		   if($send_mail==TRUE)
		   {
			   return array(true,true);
		   }
		   else
		   {
			   return array(true,false);
		   }
	   }
	   else
	   {
		   return array(false,false);
	   }
   }
   public function register_manger($user_name,$f_name,$l_name,$email,$password)
   {
	   $insert_user=$this->byzero->user_model->register_manger($user_name,$f_name,$l_name,$email,$password);
	   return $insert_user;
   }
   public function add_designer($user_name,$f_name,$l_name,$email,$password)
   {
	   $insert_user=$this->byzero->user_model->add_designer($user_name,$f_name,$l_name,$email,$password);
	   return $insert_user;
   }
     public function activate_user($key='')
	{
	     if((!empty($key))&&($key!=''))
		{
			 $check_key=$this->byzero->user_model->is_valid_reset_key($key,TRUE);
			 if($check_key)
			 {
				return $check_key;
			 }
			 else
			 {
				return FALSE;
			 }
		}
		else
		{
			return FALSE;
		}
	}
	public function portal_types($type)
	{
	  /*
		  1. User Registerd by Current Site
		  2. Using Facebook
		  3. Using Google Accounts
	  */
	  if($type=="1")
	  {
		  return site_title;
	  }
	  elseif($type=="2")
	  {
		  return "Facebook ";
	  }
	  elseif($type=="3")
	  {
		  return "Google ";
	  }
	  else
	  {
		  return false;
	  }
	}
	public function login_other_portal_user($user_data,$portal)
	{
		if($portal==3)
		{
			//google login
			$email=!empty($user_data['email'])? $user_data['email'] : FALSE;
			$profile_id=!empty($user_data['id'])? $user_data['id'] : FALSE;
			$gender=!empty($user_data['gender'])? $user_data['gender'] : '';
			$fname=!empty($user_data['given_name'])? $user_data['given_name'] : '';
			$lname=!empty($user_data['family_name'])? $user_data['family_name'] : '';
		}
		elseif($portal==2)
		{
			//facebook login
			$email=$user_data->getProperty('email');
			$profile_id=$user_data->getProperty('id');
			$gender=$user_data->getProperty('gender');
			$fname=$user_data->getProperty('name');
			$lname='';
		}
		else
		{
			return array(false,FALSE);
		}
		//var_dump($profile_id);die;
		if(!empty($profile_id))
		{
			if(empty($email))
			{
				if($portal==2)
				{
					$email=$profile_id.'@facebook.com';
				}
				else
				{
					return array(false,true,FALSE);
				}
			}
			if($this->byzero->user_model->email_check($email)==true)
			{
				// process the other portal registration
				$register_user=$this->byzero->user_model->register_other_portal_user($email,$fname,$lname,$gender,$portal,$profile_id);
				if($register_user==true)
				{
					if($this->login_portal_user($portal,$email,$profile_id)==true)
					{
						$redirect_url=$this->byzero->session->userdata('social_login_redirect');
						if(!empty($redirect_url))
						{
							$this->byzero->session->unset_userdata('social_login_redirect');
						}
						else
						{
							$redirect_url=site_url;
						}
						return array(false,true,$redirect_url);
					}
					else
					{
						return array(false,false,FALSE);
					}
				}
				else
				{
					return array(false,false,FALSE);
				}
			}
			else
			{
				//user already registred
				//process only the login
				if($this->login_portal_user($portal,$email,$profile_id)==true)
				{
					$redirect_url=$this->byzero->session->userdata('social_login_redirect');
					if(!empty($redirect_url))
					{
						$this->byzero->session->unset_userdata('social_login_redirect');
					}
					else
					{
						$redirect_url=site_url;
					}
					return array(false,true,$redirect_url);
				}
				else
				{
					return array(true,false,FALSE);
				}
			}
		}
		else
		{
			return array(false,false,false);
		}
	}
	public function get_user_id()
	{
		return $this->session->userdata['logged_in']['id'];
	}
	public function get_logged_user_details($user_id)
	{
		$user_data=$this->byzero->session->userdata['logged_in'];
	    if(!empty($user_data))
	    {
		    $user_info=$this->byzero->user->get_user_info($user_id);
			$mstatus_name=$this->byzero->user->mstatus_name($user_info['0']->mstatus);
			$location_names=$this->byzero->common->get_location_details($user_info['0']->city,$user_info['0']->state,$user_info['0']->country);
			$generate_profile_pic=array('user_portal_profile_photo'=>$this->user_profile_pic($user_id,$user_info['0']->photo_url,$user_info['0']->user_portal_id));
			if($mstatus_name==false)
			{
				$mstatus_name=array(array('mstatus_name'=>''));
			}
			if($location_names==false)
			{
				$location_names=array(array('country_id'=>'','country_name'=>'','state_id'=>'','state_name'=>'','city_id'=>'','city_name'=>''));
			}
			return (object)array_merge((array)$user_info['0'],(array)$mstatus_name['0'],(array)$location_names['0'],(array)$generate_profile_pic);
	    }
	    else
	    {
		    return FALSE;
	    }
	}
	public function get_other_user_details($user_id)
	{
		$user_data=$this->byzero->session->userdata['logged_in'];
	    if(!empty($user_data))
	    {
		    $user_info=$this->byzero->user->get_user_info($user_id);
			$mstatus_name=$this->byzero->user->mstatus_name($user_info['0']->mstatus);
			$location_names=$this->byzero->common->get_location_details($user_info['0']->city,$user_info['0']->state,$user_info['0']->country);
			$generate_profile_pic=array('user_portal_profile_photo'=>$this->user_profile_pic($user_id,$user_info['0']->photo_url,$user_info['0']->user_portal_id));
			if($mstatus_name==false)
			{
				$mstatus_name=array(array('mstatus_name'=>''));
			}
			if($location_names==false)
			{
				$location_names=array(array('country_id'=>'','country_name'=>'','state_id'=>'','state_name'=>'','city_id'=>'','city_name'=>''));
			}
			return (object)array_merge((array)$user_info['0'],(array)$mstatus_name['0'],(array)$location_names['0'],(array)$generate_profile_pic);
	    }
	    else
	    {
		    return FALSE;
	    }
	}
	public function reset_password($key='')
	{
		if((!empty($key))&&($key!=''))
		{
			 $check_key=$this->byzero->user_model->is_valid_reset_key($key,FALSE);
			 return $check_key;
		}
		else
		{
			return FALSE;
		}
	}
	public function send_forgot_password_link($email='')
   {
   		$user_detail=$this->byzero->common_model->general_select('users',array(array('field_name'=>'user_email','type'=>'where','value'=>$email)),'*');
   		if(($user_detail!=FALSE)&&(count($user_detail)==1))
   		{
			$reset_key=$this->generate_link($email);
			$reset_link=$this->generate_activation_link($reset_key,2);
			$this->byzero->load->library('user_agent');
	        $referrer=$this->byzero->agent->referrer();
			$update_reset_key=$this->byzero->common_model->general_update('users',array('user_reset_key'=>$reset_key,'user_redirect_url'=>$referrer),array('field'=>'user_email','constant'=>$email));
			if($update_reset_key==TRUE)
			{
				$subject="Pix 91 :: Password Reset Link";
				$message="<html><head></head><body><img src='".assets_url."images/logo.png' width='150' /><br/>Hi  ".ucfirst($user_detail['0']->user_fname).",<br/>We got a request from you for changing the password. ".anchor($reset_link,' Click here')." to change your password/ set your new password. If it doesn\'t works, please contact us through support@pix91.com</body></html>";
				$send_mail=$this->byzero->send_email->send_emails($email,$subject,$message);
				if($send_mail==TRUE)
				{
				   return array(true,true,true);
				}
				else
				{
				   return array(true,true,FALSE);
				}
			}
			else
			{
				return array(true,FALSE,FALSE);
			}
		}
		else
		{
			// email is not present(User not registred)
			return array(FALSE,FALSE,FALSE);
		}
     }
	public function marital_status()
	{
		$mstatus=$this->byzero->user->get_mstatus();
		if($mstatus!=false)
		{
			return $mstatus;
		}
		else
		{
			return false;
		}
	}
	public function user_profile_pic($user_id,$photo_url,$portal_url,$gender='')
	{
		if($photo_url!='')
		{
			return user_profile_photo_url.$photo_url;
		}
		elseif($portal_url!='')
		{
			return 'https://graph.facebook.com/'.$portal_url.'/picture?type=large';
		}
		elseif($gender=='male')
		{
			return user_profile_photo_url.'boy.png';
		}
		elseif($gender=='famale')
		{
			return user_profile_photo_url.'girl.png';
		}
		else
		{
			return user_profile_photo_url.'boy.png';
		}
	}
	public function get_profile_photo($user_id='0')
	{
		$user_det=$this->byzero->user_model->profile_photo($user_id);
		return $this->user_profile_pic($user_id,$user_det['0']->user_photo_url,$user_det['0']->user_portal_id,$user_det['0']->user_gender);
	}
	public function gender_list()
	{
		$genders=array(array('1','male'),array('2','female'));
		return $genders;
	}
	public function user_address($user_id)
	{
		$address=$this->byzero->user->get_mstatus_list();
		if($address!=false)
		{
			return $address;
		}
		else
		{
			return false;
		}
	}
	
	
	public function login_portal_user($portal,$email,$profile_id)
	{
		// create a session and insert the session in db
		$user_details=$this->byzero->user_model->portal_login($email);
		if($user_details==TRUE)
		{
			foreach($user_details['0'] as $key=>$data)
			{
				if($key=='user_password')
				{
					unset($user_details['0']->$key);
				}
				if(($key=='user_fname')||($key=='user_lname'))
				{
					$user_details['0']->$key=ucfirst($data);
				}
			}
			$session_data=array('user_data'=>$user_details,'last_activity'=>time(),'start_time'=>time());
			//update the Non-logged session to Logged Session
			$this->byzero->validate_login->update_to_logged_session($session_data['user_data']);
			return TRUE;
		}
		else
		{
			return false;
		}
	}
	
	
	
	
	
/*for goo water by 0007 shan*/


	public function login_otp_user($phone=0 , $supplier_id=0)
	{
		
		$user_details=$this->byzero->user_model->otp_login($phone , $supplier_id);
		
		if($user_details==TRUE)
		{
			$this->byzero->validate_login->user_det_to_session($user_details,TRUE);
			foreach($user_details['0'] as $key=>$data)
			{
				if($key=='user_password')
				{
					unset($user_details['0']->$key);
				}
				if(($key=='user_fname')||($key=='user_lname'))
				{
					$user_details['0']->$key=ucfirst($data);
				}
			}
			$session_data=array('user_data'=>$user_details,'last_activity'=>time(),'start_time'=>time());
			//update the Non-logged session to Logged Session
			$this->byzero->validate_login->update_to_logged_session($session_data['user_data']);
			return TRUE;
		}
		else
		{
			return false;
		}
	}
	
	
	
/*for goo water by 0007 shan*/
	
	
	
   public function generate_activation_link($activation_key,$type)
   {
   	  $this->byzero->load->helper(array('url','html'));
	  // $type=1 - Creating the user
	  // $type=2 - Forgetten password link
	  if($type==2)
	  {
		  return  $url=site_url."user/set_password/".$activation_key;
	  }
	  else
	  {
		  return  $url=site_url."user/activate_user/".$activation_key;
	  }
   }
   public function generate_link($email)
   {
	   $activation_key=sha1(mt_rand(10000,99999).time().$email);
	   return $activation_key;
   }
   public function profile_picture($type='1')
   {
	   //returns the profile image url
	   return true;
   }
   public function insert_session_failed()
   {
	  $this->session->sess_destroy();
	  redirect(site_url.'login', 'refresh');   
   }
   public function logout()
   {
	   $this->byzero->user->delete_session();
	   $this->session->sess_destroy();
	   redirect(site_url.'login', 'refresh');
   }
   
   	public function update_user_profile_details($user_id,$fields=NULL)
	{
		if(!empty($user_id))
		{
			$constrains=array(array('type'=>'where','field_name'=>'user_id','value'=>$user_id));
			$user_details=$this->byzero->common_model->general_select('user',$constrains,'*');
			$update_session_user_det=$this->byzero->validate_login->user_det_to_session($user_details,TRUE);
			if($update_session_user_det==TRUE)
			{
				return array(TRUE,TRUE);
			}
			else
			{
				return array(TRUE,FALSE);
			}
		}
		else
		{
			return array(FALSE,FALSE);
		}
	}
	
	public function update_admin_user_profile_details($user_id,$fields=NULL)
	{
		if(!empty($user_id)&&($fields!=NULL))
		{
			$update_user=$this->byzero->common_model->general_update('admin_users',$fields,array('field'=>'admin_user_id','constant'=>$user_id));
			if($update_user==TRUE)
			{
				$constrains=array(array('type'=>'where','field_name'=>'admin_user_id','value'=>$user_id));
				$user_details=$this->byzero->common_model->general_select('admin_users',$constrains,'*');
				$update_session_user_det=$this->byzero->validate_login->user_det_to_session($user_details,TRUE);
				return array(TRUE,TRUE);
			}
			else
			{
				return array(TRUE,FALSE);
			}
		}
		else
		{
			return array(FALSE,FALSE);
		}
	}
	public function update_admin_profile_details($user_id,$fields=NULL)
	{
		if(!empty($user_id)&&($fields!=NULL))
		{
			$update_user=$this->byzero->common_model->general_update('admin_users',$fields,array('field'=>'admin_user_id','constant'=>$user_id));
			if($update_user==TRUE)
			{
				$constrains=array(array('type'=>'where','field_name'=>'admin_user_id','value'=>$user_id));
				$user_details=$this->byzero->common_model->general_select('admin_users',$constrains,'*');
				$update_session_user_det=$this->byzero->validate_login->user_det_to_session($user_details,TRUE);
				return array(TRUE,TRUE);
			}
			else
			{
				return array(TRUE,FALSE);
			}
		}
		else
		{
			return array(FALSE,FALSE);
		}
	}
}