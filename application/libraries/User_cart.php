<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_cart
{
	private $byzero;
	public function __construct()
	{
		$this->byzero = & get_instance();
		$this->cart();
	}

	public function cart()
	{
		$class_name=$this->byzero->router->fetch_class();
		if($class_name!='image_processor')
		{
			if($this->is_cart_present()===FALSE)  	
			{
				$this->insert_array_to_cart($this->start_new_cart_session());
			}
		}
	}

	public function insert_array_to_cart($cart_array)
   	{
 		$this->byzero->session->set_userdata('cart_data',$cart_array);
 		return TRUE;
  		if($this->byzero->session->set_userdata('cart_data',$cart_array))
  		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
   	}  

	public function is_cart_present()
   	{
   	$cart_content=$this->byzero->session->userdata('cart_data');
  	if(!empty($cart_content))
  	{
  		if(!empty($cart_content['cart_items']))
  		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	else
	{
		return FALSE;
	}
   }

	public function start_new_cart_session()
	{
		$cart=$this->byzero->session->userdata('cart_data');
		$cart_items=array();$qty=array();$prices=array();$supplier=array();$item_detail=array();$sub_total=0;$discount=0;$tax=0;$coupon=0;$total=0; $pro_id=array();
		
		if(!empty($cart['pro_id']))
		{
			$pro_id=$cart['pro_id'];
		}
		if(!empty($cart['cart_items']))
		{
			$cart_items=$cart['cart_items'];
		}
		if(!empty($cart['qty']))
		{
			$qty=$cart['qty'];
		}		
		if(!empty($cart['prices']))
		{
			$prices=$cart['prices'];
		}
		if(!empty($cart['supplier']))
		{
			$supplier=$cart['supplier'];
		}
		if(!empty($cart['item_detail']))
		{
			$item_detail=$cart['item_detail'];
		}
		if(!empty($cart['sub_total']))
		{
			$sub_total=$cart['sub_total'];
		}
		if(!empty($cart['discount']))
		{
			$discount=$cart['discount'];
		}
		if(!empty($cart['tax']))
		{
			$tax=$cart['tax'];
		}
		if(!empty($cart['coupon']))
		{
			$coupon=$cart['coupon'];
		}
		if(!empty($cart['total']))
		{
			$total=$cart['total'];
		}
		
		return array('pro_id'=>$pro_id,'cart_items'=>$cart_items,'qty'=>$qty,'prices'=>$prices,'supplier'=>$supplier,'item_details'=>$item_detail,'sub_total'=>$sub_total,'discount'=>$discount,'tax'=>$tax,'coupon'=>$coupon,'total'=>$total);
	}
	
	public function add_to_cart_lib($pro_id=0 , $supp_id=0)
   	{
   		
   		$message=array();
   		$status=0;
   		$response='validation_fail';
   		$process=FALSE;
   		$i=0;
   		if(!empty($pro_id))
   		{
   			$cart=$this->byzero->session->userdata('cart_data');
   			
   			$this->byzero->load->model('fetch_model');
   			$pro_details=$this->byzero->fetch_model->fetch_pro_details_to_cart($pro_id);
   			
			if(!empty($pro_details))
   			{
				$pro_details=$pro_details[0];
				$product_name = $pro_details['pro_title']."(".$pro_details['man_name'].")";
								
				
				$cart['pro_id'][$i]=$pro_id;
				$cart['item_detail'][$pro_id]=$pro_details;
				$cart['supplier'][$supp_id]=$pro_details['supp_pro_supp_id'];
				$cart['supplier_name'][$supp_id]=$pro_details['supp_comp_name'];
				$cart['cart_items'][$pro_id]=$product_name;
				$cart['prices'][$pro_id]=$pro_details['supp_pro_price'];
				
				
				if(!empty($cart['qty'][$pro_id]))
				{					
					$cart['qty'][$pro_id]=$cart['qty'][$pro_id]+1;
				}
				else
				{
					$cart['qty'][$pro_id]=1;
				}
				$i++;
					
				$cal_item_prices = $this->calculate_item_prices($pro_id , $cart);
				$cart['item_price'][$pro_id] = $cal_item_prices['item_price'];
				
				$save_cart = $this->calculate_cart_total_amount($cart);
				
			
				if($save_cart)
				{
					$status=1;$message[]='Your Product Added Successfully';$response='success';
				}
				else
				{
					$message[]='Server Error';$response='server_error';
				}
			}
			else
			{
				$message=array('Inavlid Product');
			}	
		}
		else
		{
			$message=array('Inavlid Product');
		}
		return array('status'=>$status,'message'=>$message,'response'=>$response);
   	}

	public function update_cart_lib($pro_id=0 , $supp_id=0 , $quantity=0)
   	{
   		
   		$message=array();
   		$status=0;
   		$response='validation_fail';
   		$process=FALSE;
   		
   		if(!empty($pro_id))
   		{
   			$cart=$this->byzero->session->userdata('cart_data');
   			
   			$this->byzero->load->model('fetch_model');
   			$pro_details=$this->byzero->fetch_model->fetch_pro_details_to_cart($pro_id);
   			
			if(!empty($pro_details))
   			{
			
				$cart['qty'][$pro_id]=$quantity;
				
				$cal_item_prices = $this->calculate_item_prices($pro_id , $cart);
				$cart['item_price'][$pro_id] = $cal_item_prices['item_price'];
				
				$save_cart = $this->calculate_cart_total_amount($cart);
				
			
				if($save_cart)
				{
					$status=1;$message[]='Your Product Added Successfully';$response='success';
				}
				else
				{
					$message[]='Server Error';$response='server_error';
				}
			}
			else
			{
				$message=array('Inavlid Product');
			}	
		}
		else
		{
			$message=array('Inavlid Product');
		}
		return array('status'=>$status,'message'=>$message,'response'=>$response);
   	}

	public function delete_product_from_cart_lib($pro_id=0 , $supp_id=0)
   	{
   		
   		$message=array();
   		$status=0;
   		$response='validation_fail';
   		$process=FALSE;
   		
   		if(!empty($pro_id))
   		{
   			$cart=$this->byzero->session->userdata('cart_data');
   			
   			$this->byzero->load->model('fetch_model');
   			$pro_details=$this->byzero->fetch_model->fetch_pro_details_to_cart($pro_id);
   			
			if(!empty($pro_details))
   			{
				$pro_details=$pro_details[0];
				$product_name = $pro_details['pro_title']."(".$pro_details['man_name'].")";
				
				unset($cart['item_detail'][$pro_id]);
				unset($cart['cart_items'][$pro_id]);
				unset($cart['prices'][$pro_id]);
				unset($cart['qty'][$pro_id]);
				
				$cart['supplier'][$supp_id]=$pro_details['supp_pro_supp_id'];
			
				$cal_item_prices = $this->calculate_item_prices($pro_id , $cart);
				$cart['item_price'][$pro_id] = $cal_item_prices['item_price'];
				
				$save_cart = $this->calculate_cart_total_amount($cart);
				
			
				if($save_cart)
				{
					$status=1;$message[]='Product Removed Successfully';$response='success';
				}
				else
				{
					$message[]='Server Error';$response='server_error';
				}
			}
			else
			{
				$message=array('Inavlid Product');
			}	
		}
		else
		{
			$message=array('Inavlid Product');
		}
		return array('status'=>$status,'message'=>$message,'response'=>$response);
   	}
   
   	public function calculate_cart_total_amount($cart=FALSE)
   	{
   		
   		if(empty($cart))
   		{
			$cart=$this->byzero->session->userdata('cart_data');
		}
		
		$price = $cart['prices'];
		$quantity = $cart['qty'];
   		
   		$temp_tot = 0;
   		foreach($price as $key=>$item)
   		{
			$temp_quan = $quantity[$key];
			$temp_tot += $item * $temp_quan;
			
		}
		$cart['total'] = $temp_tot;
		return $this->insert_array_to_cart($cart);
		//var_dump($this->byzero->session->userdata('cart_data'));
		
	}
	
	public function calculate_item_prices($pro_id=0 , $cart=FALSE)
   	{
   		if(empty($cart))
   		{
			$cart=$this->byzero->session->userdata('cart_data');
		}
		
		$price = 0;
		if(!empty($cart['item_detail'][$pro_id]['supp_pro_price']))
		{
			$price = $cart['item_detail'][$pro_id]['supp_pro_price'];
		}
		
		$quantity = 0;
		if(!empty($cart['qty'][$pro_id]))
		{
			$quantity = $cart['qty'][$pro_id];
		}
		
		$item_price = $quantity * $price;
		
		return array('item_price' => $item_price);
	}
	
 	public function destory_cart()
   	{
  		$this->byzero->session->unset_userdata('cart_data');
  		return TRUE;
   	}

   
}