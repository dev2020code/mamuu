<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Orders_lib 
{
	private $byzero;
	public function __construct()
	{
		$this->byzero = & get_instance();
	}
	public function orders_list($status=FALSE,$search_data=FALSE)
	{
		$order_list=$this->byzero->data_tbl_retrive_data->fetch_orders_list($status,$search_data);
		//var_dump($order_list); die;
		if(!empty($order_list))
		{
			$order_count=count($order_list);
		}
		else
		{
			$order_list=FALSE;
			$order_count=0;
		}
		return array('orders_list'=>$order_list,'orders_count'=>$order_count);
	}
	
	public function sending_msg_prep($ord_id,$paying_now=0)
	{
		$msg_details = $this->byzero->common_model->__fetch_contents('order_master',array('order_master_id'=>$ord_id , 'order_master_removed'=>'0'));

		
		$order_no=$msg_details[0]['order_master_no'];
		$customer_name=$msg_details[0]['order_master_client_name'];
		$customer_phone=$msg_details[0]['order_master_client_contact_phone'];
		$cake_price=$msg_details[0]['order_master_tot_to_pay'];
		$paid_till=$msg_details[0]['order_master_paid_till'];
		$bal_to_pay=$msg_details[0]['order_master_bal_to_pay'];
		$cake_status=$msg_details[0]['order_master_status'];
		$msg='';
		
		if($cake_status==2)
		{
			$msg="Dear ".$customer_name." Your Order is Confirmed. Your Order No is ".$order_no;
		}
		if($cake_status==3)
		{
			$msg="Dear ".$customer_name." Your Order No:".$order_no." Ready To Deliver";
		}
		if($cake_status==4)
		{
			$msg="Dear ".$customer_name." Thank You Visit Us Again.";
		}
		
		if($cake_status==5)
		{
			$msg="Dear ".$customer_name." Thank You Visit Us Again. Balance To Pay is ".$bal_to_pay;
		}
		else
		{
			if($paying_now>0)
			{
				$msg.=' We have Received Payment of '.$paying_now.'. ';
				if($bal_to_pay>0)
				{
					$msg.="Balance To Pay is ".$bal_to_pay;
				}
			}
		}
		//var_dump($msg);
		if(!empty($customer_phone))
		{
			if(is_numeric($customer_phone)&&(strlen($customer_phone)==10))
			{
				$this->send_msg($customer_phone,$msg);
			}
		}
		
		
	}
	
	
	public function send_msg($number='',$msg='')
	{
	
		//var_dump($number);var_dump($msg);
		$msg1=urlencode($msg);
		$url = 'http://sms.byzerotechnologies.com/api/sendmsg.php?user='.sms_user_name.'&pass='.sms_password.'&sender='.sms_sender_id.'&phone='.$number.'&text='.$msg1.'&priority=ndnd&stype=normal';
		//echo($url);
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 200);
		$curl_scraped_page = curl_exec($ch);
		//echo $curl_scraped_page;
		curl_close($ch);
		return TRUE;
		
	}
}