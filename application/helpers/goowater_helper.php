<?php
function get_front_user_left_menus($user_id , $user_group)
{
	$u_id = $user_id;
	$u_grp = $user_group;
	
	if($u_grp == 3)
	{
		$profile_path = site_url.'supply_list/customer_profile_edit/'.$u_id;
		$order_path = site_url.'supply_list/orders_list/'.$u_id;
		$address_path = site_url.'supply_list/customer_address_edit/'.$u_id;
		$name_path = site_url.'supply_list/customer_name_edit/'.$u_id;
		return '<ul class="order_profile_left_menu_ul">
	
            	<li>
            		<a href="'.$order_path.'" class="order_profile_left_menu_li">Orders</a>
            	</li>
            	
            	<li>
                    <a href="'.$name_path.'" class="order_profile_left_menu_li">Profile</a>
                </li>
                
                <li>
                    <a href="'.$profile_path.'" class="order_profile_left_menu_li">Contact</a>
                </li>
            	
                <li>
                    <a href="'.$address_path.'" class="order_profile_left_menu_li">Address</a>
                </li>
                
                
                
	        </ul>';
	}
	
	if($u_grp == 2)
	{
		$pending_order = site_url.'supply_list/supplier_pending_orders/';
		$order_history = site_url.'supply_list/orders_list/'.$u_id;
		$edit_profile = site_url.'supply_list/customer_profile_edit/'.$u_id;
		$add_product = site_url.'supply_list/supplier_products_adding/'.$u_id;
		$add_serv_and_feat = site_url.'supply_list/customer_service_feature_edit/'.$u_id;
		$address_path = site_url.'supply_list/customer_address_edit/'.$u_id;
		return '<ul class="order_profile_left_menu_ul">
	
            	<li>
            		<a href="'.$pending_order.'" class="order_profile_left_menu_li">Pending</a>
            	</li>
            	
            	<li>
            		<a href="'.$order_history.'" class="order_profile_left_menu_li">Orders </a>
            	</li>
            	
            	<li>
            		<a href="'.$edit_profile.'" class="order_profile_left_menu_li">Profile</a>
            	</li>
            	
            	<li>
                    <a href="'.$add_product.'" class="order_profile_left_menu_li">Products</a>
                </li>
                
                <li>
                    <a href="'.$add_serv_and_feat.'" class="order_profile_left_menu_li">Service</a>
                </li>
            	
                <li>
                    <a href="'.$address_path.'" class="order_profile_left_menu_li">Address</a>
                </li>
                
                
                
	        </ul>';
	}
	
	
}

				
