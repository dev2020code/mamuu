<html>
<head>
    <title>Mamuu Expo- Contest Programme</title>
    <link rel="icon" href="<?= assets_url?>/mamuu/logo.png" type="image/gif" sizes="16x16">
   <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&amp;display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=PT+Serif:wght@400;700&amp;display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@500&amp;display=swap" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?= assets_url?>css/style.css">
 <script src="<?= assets_url?>js/myscripts.js"></script>  
 <style type="text/css">
small {
    margin-top: -11px;
    display: block;
    margin-bottom: 10px;
}
.modal{
  z-index: 200000;
}
#loader{
  z-index: 2000000 !important;
}
/*.text-danger {
    color: #fff !important;
}*/

</style>
</head>

<body>
<section>
   <div id="loader"></div>
<div class="slider">
<div class="site-header">
 <!-- <iframe src="https://www.youtube.com/embed/m4ICyR3QTR0?autoplay=1&amp;mute=1&amp;loop=1&amp;rel=0&amp;showinfo=0&amp;showsearch=0&amp;controls=0&amp;playlist=m4ICyR3QTR0" allowfullscreen="" frameborder="0"></iframe> -->
 <video width="100%"  autoplay=""  muted="" >
        <source src="http://demo.getln.com/mamuu/06/assets/mamuu/vid.mp4">
</video>
 <!-- <div class="video-overlay"></div> -->
 
  <div class="content">
    <h1>MAMUU EXPO MARKET LLP</h1>
     <h4>Research & Development</h4>
      <h4>Online Trading</h4>
       <h4>MCX - FOREX</h4>
  </div>
</div>
</div>

<div class="sec-a">
<div class="container"> 
<div class="row">
<!-- <div class="col-md-5 col-sm-5 col-xs-12">
<div class="con-form">
<form id="upload">
  <div class="container">
    <h3>Mamuu Expo Offers</h3>
    <h2>Live Trading Signal</h2>
    <h3>Join Mammu Expo Today</h3>
    <hr>
<input type="hidden" name="<?php echo  $this->security->get_csrf_token_name(); ?>" id="csrf_token" value="<?php echo  $this->security->get_csrf_hash(); ?>" />
   <input type="text" placeholder="Name" name="fname" id="fname">
   <br><small><span class="text-danger"></span></small>
    <input type="tel" placeholder="Contact Number" id="phone" name="phone" required>
    <br><small><span class="text-danger" ></span></small>
 
  <input type="text" placeholder="E mail" name="email" id="email" required>
  <br><small><span class="text-danger" ></span></small>

     <button type="submit" class="signupbtn">Get Started</button>
  </div>
</form>
</div>
</div> -->
<div class="col-md-12 col-sm-12 col-xs-12 block_anim before">
<div class="wel-txt">
<p><span>MAMUU Expo Market LLP - </span> Leading research and development (Online trading tool) with success strategy. Tool support for MCX – SHARE MARKET – NIFFTY and Any online trading instruments. Our Business: IMPORT & EXPORT – Garments / BUSINESS NETWORK ORGINATION – Referral Corporate Business Leads / FUND MANNAGEMENT SERVICE – Investment Consulting / SOCIAL SERVICE ACTIVITES. <span class="cmpany-pro">Established in 2011</span></p>
</div>
</div>

</div>
</div>
</div>

<div class="sec-d">
<div class="container">
<div class="row">
<div class="col-md-12">
<h2>OUR BUSINESS</h2>
</div>
</div>
<div class="row">
<div class="col-md-4 col-sm-12 col-xs-12 no-pad" id="mpad1">
<div data-aos="flip-left">
<div class="sec-d1-txt">
<div class="sec-d1-img">
<a href="https://www.mamuuexpo.com/" target="_blank"> <img src="<?php echo assets_url?>mamuu/imaex.jpg" id="im1"> </a>
<div class="hov2">
	<p id="hov1"><a href="https://www.mamuuexpo.com/" target="_blank"> Export / Import </a></p>
</div>
</div>
<div class="busi">
<h3><a href="https://www.mamuuexpo.com/" target="_blank"> Export / Import</a></h3>
<p><a href="https://www.mamuuexpo.com/" target="_blank"> Garments</a></p>
</div>
</div>
</div>
</div>
<div class="col-md-4 col-sm-12 col-xs-12 no-pad" id="mpad1">
<div data-aos="flip-up">
<div class="sec-d2-txt">
<div class="sec-d2-img">
<a href="https://www.moutfitters.com/" target="_blank"><img src="<?php echo assets_url?>mamuu/outfit.jpg" id="im1"></a>
<div class="hov2">
	<p id="hov1"><a href="https://www.moutfitters.com/" target="_blank">Retail Outlet</a></p>
</div>
</div>
<div class="busi">
<h3><a href="https://www.moutfitters.com/" target="_blank">Retail Outlet</a></h3>
<p><a href="https://www.moutfitters.com/" target="_blank">Telecom Product</a></p>
</div>
</div>
</div>
</div>
<div class="col-md-4 col-sm-12 col-xs-12 no-pad" id="mpad1">
<div data-aos="fade-up-right">
<div class="sec-d3-txt">
<div class="sec-d3-img">
<a href="https://mamuuforex.com/" target="_blank"><img src="<?php echo assets_url?>mamuu/ont.jpg" id="im1"></a>
<div class="hov2">
	<p id="hov1"><a href="https://mamuuforex.com/" target="_blank">Research & Development</a></p>
</div>
</div>
<div class="busi">
<h3><a href="https://mamuuforex.com/" target="_blank">Research & Development</a></h3>
<p><a href="https://mamuuforex.com/" target="_blank">Online Trading Tools</a></p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="sec-h">
<div class="container">
<div class="row">
<div class="col-md-12">
<h2>OUR SERVICES</h2>
</div>
</div>
<div class="row">
<div class="col-md-4 col-sm-12 col-xs-12 no-pad" id="mpad">
<div data-aos="flip-left">
<div class="sec-d4-txt">
<div class="sec-d4-img">
<a href="https://mamuuforex.com/" target="_blank"><img src="<?php echo assets_url?>mamuu/lt.jpg" id="im"></a>
<div class="hov">
	<p id="hov1"><a href="https://mamuuforex.com/" target="_blank">Live Trading Signal Provider </a></p>
</div> 
</div>
<div class="busi1">
<p><a href="https://mamuuforex.com/" target="_blank">Live Trading Signal Provider </a> </p>
</div>
</div>
</div>
</div>
<div class="col-md-4 col-sm-12 col-xs-12 no-pad" id="mpad">
<div data-aos="flip-left">
<div class="sec-d4-txt">
<div class="sec-d4-img">
<a href="https://mamuuforex.com/" target="_blank"><img src="<?php echo assets_url?>mamuu/fund1.jpg" id="im"></a>
<div class="hov">
	<p id="hov1"><a href="https://mamuuforex.com/" target="_blank">Fund Management Service </a></p>
</div>
</div>
<div class="busi1">
<p><a href="https://mamuuforex.com/" target="_blank">Fund Management Service  </a></p>
</div>
</div>
</div>
</div>
<div class="col-md-4 col-sm-12 col-xs-12 no-pad" id="mpad">
<div data-aos="flip-left">
<div class="sec-d4-txt">
<div class="sec-d4-img">
<a href="https://mamuuforex.com/" target="_blank"><img src="<?php echo assets_url?>mamuu/ots.jpg" id="im"></a>
<div class="hov">
	<p id="hov1"><a href="https://mamuuforex.com/" target="_blank">Trading Tool Development </a></p>
</div>
</div>
<div class="busi1">
<p><a href="https://mamuuforex.com/" target="_blank">Trading Tool Development </a></p>
</div>
</div>
</div>
</div>


<div class="col-md-4 col-sm-12 col-xs-12 no-pad" id="mpad">
	<div class="tec">
<div data-aos="flip-left">
<div class="sec-d4-txt">
<div class="sec-d4-img">
<a href="https://mamuuforex.com/career/" target="_blank"><img src="<?php echo assets_url?>mamuu/carr.jpg" id="im"></a>
<div class="hov">
	<p id="hov1"><a href="https://mamuuforex.com/career/" target="_blank">Career Opportunity</a> </p>
</div>
</div>
<div class="busi1">
<p><a href="https://mamuuforex.com/career/" target="_blank">Career Opportunity </a></p>
</div>
</div>
</div>
</div>
</div>
<div class="col-md-4 col-sm-12 col-xs-12 no-pad" id="mpad">
<div class="tec">
<div data-aos="flip-left">
<div class="sec-d4-txt">
<div class="sec-d4-img">
	<div class="tra">
<a href="http://unmaiillamtrust.in/" target="_blank"><img src="<?php echo assets_url?>mamuu/ss.jpg" id="im"></a>
<div class="hov">
	<p id="hov1"><a href="http://unmaiillamtrust.in/" target="_blank">Social Service Activities </a></p>
</div>
</div>

</div>
<div class="busi1">
<p><a href="http://unmaiillamtrust.in/" target="_blank">Social Service Activities </a></p>
</div>
</div>
</div>
</div>
</div>

<div class="col-md-4 col-sm-12 col-xs-12 no-pad" id="mpad">
<div class="tec">
<div data-aos="flip-left">
<div class="sec-d4-txt">
<div class="sec-d4-img">
	<div class="tra">
<img src="<?php echo assets_url?>mamuu/glo.jpg" id="im">
<div class="hov">
	<p id="hov1">Corporate Global Business <br> Network</p>
</div>
</div>
</div>
<div class="busi1">
<p>Corporate Global Business Network</p>
</div>
</div>
</div>
</div>
</div>
</section>
<div class="modal fade" id="mymodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-title" id="title"></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p id="result" style="color: #555;"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">Ok</button>
        
      </div>
    </div>
  </div>
</div>
<form id="startupload">
<div class="modal fade" id="startmodal" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        
        <h4 class="modal-startupload" id="title">Join Mammu Expo Today</h4>
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> -->
      </div>
      <div class="modal-body">
        <input type="hidden" name="<?php echo  $this->security->get_csrf_token_name(); ?>" id="csrf_token" value="<?php echo  $this->security->get_csrf_hash(); ?>" />
        <input type="text" placeholder="Name" name="fname" id="fname">
        <br><small><span class="text-danger"></span></small>
        <input type="tel" placeholder="Contact Number" id="phone" name="phone" required>
        <br><small><span class="text-danger" ></span></small>
     
        <input type="text" placeholder="E mail" name="email" id="email" required>
        <br><small><span class="text-danger" ></span></small>
        <label class="form-label">Are you a trader : </label> <br>
        <input type="radio" name="forex" value="Forex" checked> Forex 
        <input type="radio" name="forex" value="MCX"> MCX
        <input type="radio" name="forex" value="None"> None
        <br><small><span class="text-danger" ></span></small>
      </div>
      <div class="modal-footer">
        <button type="submit" class="signupbtn" >Get Started</button>
        
      </div>
    </div>
  </div>
</div>
</form>

</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#startmodal').modal('show');
    
  });
  $( "#startupload" ).on( "submit", function(){
  $('.text-danger').html(' ');
    $('#loader').show();
    $.ajax({
            type: "POST",
            url: "<?php echo site_url; ?>home/add_start_process",
          dataType: "json",
            data: $("#startupload").serialize(),
  
            success: function(html){
              if(html.status==0)
                {
                  //event.preventDefault();
                  $('.form-control').removeClass('is-invalid');
                  var s=0;
                  // $("#startupload [name='"+k+"']+br+small span"),html('');
                  $.each(html.message, function(k,v) {
                          if(s== 0)
                          {
                            $("#startupload [name='"+k+"']").focus();
                            s=1;
                          }

                         $("#startupload [name='"+k+"']+br+small span").html(v);
                         $("#startupload [name='"+k+"']").addClass( "is-invalid" );
                         // alert(k);


                  });
                 // $('#error_add').html('<div id="msg" class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i>'+err+'</div>');
                }
                else
                {
                $('#loader').hide();
                /*$('#title').html('Success');
                $('#result').html(html.message);
                $('#mymodal').modal('show');
                */
                $('#startmodal').modal('hide');
                $('#startmodal').on('hidden.bs.modal', function () {
                    // location.reload();
                  });
                }
            },
            complete: function (data) {

                      $('#loader').hide();

            },
            error: function(jqXHR, textStatus, errorThrown) { alert('Please try again'); }
       });
      
        return false;
    });

</script>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="foo-1">
                    <h4>REGISTER OFFICE</h4>
                    <div class="foo-mail">
                     <p><img src="<?php echo assets_url?>mamuu/mail.png"><a href="mailto:info@mamuuexpogroup.com">info@mamuuexpogroup.com</a></p>
                     </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                    <div class="foo-2">
                        <h4>OUR BUSINESS</h4>
                        <ul>
                        <li> <a href="https://www.mamuuexpo.com/" target="_blank">Export / Import </a></li>   
                        <li><a href="https://www.moutfitters.com/" target="_blank">Retail Outlet </a></li>
                        <li><a href="https://mamuuforex.com/" target="_blank">Research & Development </a></li>
                        </ul>
                        </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12">
                     <div class="foo-3">
                        <h4>OUR SERVICES</h4>
                        <ul>
                        <li><a href="https://mamuuforex.com/" target="_blank"> Live Trading Signal Provider  </a></li>   
                        <li><a href="https://mamuuforex.com/" target="_blank">Fund Management Service   </a></li>
                        <li><a href="https://mamuuforex.com/" target="_blank">Trading Tool Development </a></li>
                        <li><a href="https://mamuuforex.com/career/" target="_blank"> Career Opportunity </a></li>   
                        <li><a href="http://unmaiillamtrust.in/" target="_blank">Social Service Activities   </a></li>
                        <li><a href="#" target="_blank">Corporate Global Business Network </a></li>
                        </ul>
                        </div>
                </div>
            </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="cpy-right">
                    <p>Copyright@2020 All Rights Reserved |<a href="#"> Mamuu Expo Market LLP </a></p>
                </div>
                </div>
          
            </div>
        </div>
</footer>

</body>
</html>
