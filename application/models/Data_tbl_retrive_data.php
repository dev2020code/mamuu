<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
Class Data_tbl_retrive_data extends CI_Model
{

	public function get_complaints($data,$complaint_id=0)
	{
		$start_row=0;
		if(isset($data['offset']))
		{
			$start_row=$data['offset'];
		}
		$this->db->select('a.*,b.dept_name,d.user_type_name,c.year_name,IF(1>2, 0, '.$start_row.') AS start,DATE_FORMAT(a.cd_date,\'%d - %M - %Y\') as date,DATE_FORMAT(a.cd_completed_date,\'%d - %M - %Y\') as completed_date',FALSE);
		$this->db->from('complaint_data as a');
		$this->db->join('departments as b' , 'a.cd_dept=b.dept_id AND b.dept_removed=0' , 'LEFT');
		$this->db->join('year as c' , 'a.cd_year=c.year_id AND c.year_removed=0' , 'LEFT');
		$this->db->join('user_types as d' , 'a.cd_user_type=d.user_type_id AND d.user_type_removed=0' , 'LEFT');

		if(!empty($data['search']))
		{
			$this->db->where('(`cd_name`  LIKE \'%'.$data['search'].'%\')',NULL,FALSE);
		}
		if(!empty($data['order']))
		{
			if(!empty($data['sort']))
			{
				$this->db->order_by($data['sort'],$data['order']);
			}
			else
			{
				$this->db->order_by('a.cd_id','desc');
			}
		}

		if(!empty($data['offset']))
		{
			$this->db->offset($data['offset']);
		}

		if(!empty($data['limit']))
		{
			$this->db->limit($data['limit']);
		}
		if($data['rpt_status']!='')
		{
			if($data['rpt_status']!='null')
			{
				$this->db->where('cd_status',$data['rpt_status']);
			}
		}
		if(!empty($complaint_id))
		{
			$this->db->where('cd_id',$complaint_id);
		}
		$this->db->where('cd_removed',0);
		$query = $this -> db -> get();
		//var_dump($this->db->last_query());
	  	if($query -> num_rows())
	   	{
	   	 	return $query->result_array();
		}
	   	else
	   	{
		 	return false;
	   	}
	}
	public function complaints_count($status=0)
	{

		$start_row=0;
		if(isset($data['offset']))
		{
			$start_row=$data['offset'];
		}
		$this->db->select('count(cd_id) as count');
		$this->db->from('complaint_data as a');
		$this->db->where('cd_status',$status);
		$this->db->where('cd_removed',0);
		$query = $this -> db -> get();
		//var_dump($this->db->last_query());
	  	if($query -> num_rows())
	   	{
	   	 	return $query->result_array();
		}
	   	else
	   	{
		 	return false;
	   	}
	}

	public function get_complaints_count($data)
	{
		$this->db->select('count(cd_id)as tot');
		$this->db->from('complaint_data');

		if(!empty($data['search']))
		{
			$this->db->where('(`cd_name`  LIKE \'%'.$data['search'].'%\')',NULL,FALSE);

		}
		if($data['rpt_status']!='null')
		{
			$this->db->where('cd_status',$data['rpt_status']);
		}
		$this->db->where('cd_removed',0);
		$query = $this -> db -> get();
		//var_dump($this->db->last_query());
	   	if($query -> num_rows())
	   	{
	   	 	return $query->result_array();
	   	}
	   	else
	   	{
			return false;
	   	}
		}



}
