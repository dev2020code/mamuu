<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
Class Fetch_model extends CI_Model
{
	/**
	* BYZ0007
	* @return
	*/

	public function check_otp_existence($str)
	{

		$this->db->select('a.user_reg_otp_password');
	   	$this->db->from('user_register as a');
	    $this->db->where('a.user_reg_otp_password',$str);

	   	$query = $this -> db -> get();

	   	if($query -> num_rows() === 0)
	   	{
	   	 	return TRUE;
		}
	   	else
	   	{
		 	return FALSE;
	   	}
	}

	public function insert_user_table($data1,$data2)
	{
		//var_dump($data1,$data2); die;
		$this->db->trans_begin();

		$this->db->insert('user',$data2);
		$lastid=$this->db->insert_id();

		if(!empty($lastid))
		{
			$data1['user_reg_user_id']=$lastid;
			$this->db->insert('user_register',$data1);
			$lastid_1=$this->db->insert_id();
		}

		if ($this->db->trans_status() === FALSE)
		{
		    $this->db->trans_rollback();
		    return FALSE;
		}
		else
		{
		    $this->db->trans_commit();
		    return $lastid_1;
		}
	}

	public function insert_company_details($comp_details , $image_data , $tag_data , $social_data , $yt_url_data)
	{
		$this->db->trans_begin();

		$this->db->insert('company_details',$comp_details);
		$lastid=$this->db->insert_id();

		if(!empty($image_data))
		{
			foreach($image_data as $key=>$item)
			{
			$image_data[$key]['comp_img_comp_id']=$lastid;
			$this->db->insert('company_photos',$image_data[$key]);
			}
		}

		if(!empty($tag_data))
		{
			foreach($tag_data as $key=>$item2)
			{
			$tag_data[$key]['comp_tag_comp_id']=$lastid;
			$this->db->insert('company_tags',$tag_data[$key]);
			}
		}

		if(!empty($social_data))
		{
			foreach($social_data as $key=>$item3)
			{
			$social_data[$key]['comp_sl_comp_id']=$lastid;
			$this->db->insert('company_social_links',$social_data[$key]);
			}
		}

		if(!empty($yt_url_data))
		{
			foreach($yt_url_data as $key=>$item2)
			{
			$yt_url_data[$key]['yt_link_comp_id']=$lastid;
			$this->db->insert('company_youtube_links',$yt_url_data[$key]);
			}
		}

		if ($this->db->trans_status() === FALSE)
		{
		    $this->db->trans_rollback();
		    return FALSE;
		}
		else
		{
		    $this->db->trans_commit();
		    return $lastid;
		}
	}

	public function update_company_details($comp_details , $image_data , $tag_data , $social_data , $id , $yt_url_data)
	{
		$this->db->trans_begin();
		
		$comp_id['comp_id'] = $id;
		$this->db->where($comp_id);
		$this->db->update('company_details',$comp_details);

		if(!empty($image_data))
		{
			foreach($image_data as $key=>$item)
			{
			$image_data[$key]['comp_img_comp_id']=$id;
			$this->db->insert('company_photos',$image_data[$key]);
			}
		}

		if(!empty($tag_data))
		{
			$tag_id['comp_tag_comp_id'] = $id;
			$this->db->where($tag_id);
			$this->db->delete('company_tags');
			foreach($tag_data as $key=>$item2)
			{
			$tag_data[$key]['comp_tag_comp_id']=$id;
			$this->db->insert('company_tags',$tag_data[$key]);
			}
		}

		if(!empty($social_data))
		{
			$sl_id['comp_sl_comp_id'] = $id;
			$this->db->where($sl_id);
			$this->db->delete('company_social_links');
			foreach($social_data as $key=>$item3)
			{
			$social_data[$key]['comp_sl_comp_id']=$id;
			$this->db->insert('company_social_links',$social_data[$key]);
			}
		}

		if(!empty($yt_url_data))
		{
			$yt_url_id['yt_link_comp_id'] = $id;
			$this->db->where($yt_url_id);
			$this->db->delete('company_youtube_links');
			foreach($yt_url_data as $key=>$item4)
			{
			$yt_url_data[$key]['yt_link_comp_id']=$id;
			$this->db->insert('company_youtube_links',$yt_url_data[$key]);
			}
		}

		if ($this->db->trans_status() === FALSE)
		{
		    $this->db->trans_rollback();
		    return FALSE;
		}
		else
		{
		    $this->db->trans_commit();
		    return $id;
		}
	}

	public function fetch_social_link_to_edit($id)
	{

		$this->db->select('a.*,b.social_title,b.social_img,b.social_id,b.social_url');
    $this->db->from('company_social_links as a');
    $this->db->join('social_links as b','a.comp_sl_linl=b.social_id AND social_removed=0');
    $this->db->where('a.comp_sl_removed','0');

    if(!empty($id))
    {
			$this->db->where('a.comp_sl_comp_id',$id);
		}
	  $query = $this -> db -> get();
		//print_r($this->db->last_query());
		if($query -> num_rows())
		{
		 	return $query->result_array();
		}
		else
		{
		 	return false;
		}
	}


	public function get_company_info($data)
	{
		//GROUP_CONCAT(artists.artistname SEPARATOR '----')
		//$this->db->select('a.*,b.state_name,c.city_name,d.cat_name,e.sub_cat_name,GROUP_CONCAT(f.comp_img_name SEPARATOR ",")as photos,GROUP_CONCAT(k.yt_link_urls SEPARATOR ",")as youtubes,GROUP_CONCAT(h.tag_name SEPARATOR ",")as tags,GROUP_CONCAT(i.comp_sl_link_url SEPARATOR ",")as social_link,GROUP_CONCAT(j.social_title SEPARATOR ",")as social_heading');

		$this->db->select('a.*,b.state_name,c.city_name,d.cat_name,e.sub_cat_name,GROUP_CONCAT(h.tag_name SEPARATOR ",")as tags');
		$this->db->from('company_details as a');
	    $this->db->join('state as b','a.comp_state=b.state_id','LEFT');
	    $this->db->join('city as c','a.comp_city=c.city_id','LEFT');
	    $this->db->join('category as d','a.comp_category=d.cat_id','LEFT');
	    $this->db->join('subcategory as e','a.comp_sub_category=e.sub_cat_id','LEFT');
	  	$this->db->join('company_tags as g','a.comp_id=g.comp_tag_comp_id AND g.comp_tag_removed=0','LEFT');
	    $this->db->join('tags as h','g.comp_tag_name=h.tag_id','LEFT');

	    /*$this->db->join('company_photos as f','a.comp_id=f.comp_img_comp_id AND f.comp_img_removed=0','LEFT');
	    $this->db->join('company_youtube_links as k','a.comp_id=k.yt_link_comp_id','LEFT');
	    $this->db->join('company_tags as g','a.comp_id=g.comp_tag_comp_id AND g.comp_tag_removed=0','LEFT');
	    $this->db->join('tags as h','g.comp_tag_name=h.tag_id','LEFT');
	    $this->db->join('company_social_links as i','a.comp_id=i.comp_sl_comp_id','LEFT');
	    $this->db->join('social_links as j','i.comp_sl_linl=j.social_id','LEFT');
	    */

	    if(!empty($data['comp_id']))
    	{
			$this->db->where('a.comp_id',$data['comp_id']);
		}
		//$this->db->group_by('tags,youtubes');

		$this->db->where('a.comp_removed',0);

		$query = $this -> db -> get();
	   	if($query -> num_rows())
	   	{
	   	 	return $query->result_array();
	   	}
	   	else
	   	{
		 	return false;
	   	}
	}







}
