<?php
Class User_model extends CI_Model
{
	public function username_check($username)
	{
		$this -> db -> select('*');
		$this -> db -> from('users');
		$this -> db -> where('user_name',$username);
		$query= $this -> db -> get();
		if($query -> num_rows() == 0)
	    {
		  return true;
	    }
	    else
	    {
		  return false;
	    }
	}
	public function email_check($email)
	{
		$this -> db -> select('*');
		$this -> db -> from('users');
		$this -> db -> where('user_email',$email);
		$query= $this -> db -> get();
		if($query -> num_rows() == 0)
	    {
		  return true;
	    }
	    else
	    {
		  return false;
	    }
	}
	public function register_user($f_name,$l_name,$email,$password,$activation_key='',$referrer='')
	{
		$date=date('Y-m-d');
		$data=array(//'user_name'=>$this->db->escape_str($user_name),
				    'user_password'=>$this->db->escape_str(md5($password)),
				    'user_fname'=>$this->db->escape_str($f_name),
				    'user_lname'=>$this->db->escape_str($l_name),
				    'user_email'=>$this->db->escape_str($email),
				    'user_group'=>'2',
				    'user_verified'=>'0',
				    'user_reset_key'=>$activation_key,
				    'user_portal'=>'1',
				    'user_created'=>$this->db->escape_str($date),
				    'user_redirect_url'=>$referrer);
	   $this->db->trans_begin();
	   $insert=$this->db->insert('users', $data); 
	   $user_id=$this->db->insert_id();
	   if ($this->db->trans_status() === FALSE)
	   {
		  $this->db->trans_rollback();
		  return false;
	   }
	   else
	   {
		  if($this->db->trans_commit())
		  {
			  return $user_id;
		  }
		  else
		  {
			  return false;
		  }
	   }
	}
	public function add_manger($user_name,$f_name,$l_name,$email,$password)
	{
		$date=date('Y-m-d');
		$data=array('admin_user_name'=>$this->db->escape_str($user_name),
				    'admin_user_password'=>$this->db->escape_str(md5($password)),
				    'admin_user_fname'=>$this->db->escape_str($f_name),
				    'admin_user_lname'=>$this->db->escape_str($l_name),
				    'admin_user_email'=>$this->db->escape_str($email),
				    'admin_user_group'=>'3',
				    'admin_user_verified'=>'0',
				    'admin_user_created'=>$this->db->escape_str($date));
	   $this->db->trans_begin();
	   $insert=$this->db->insert('admin_users', $data); 
	   if ($this->db->trans_status() === FALSE)
	   {
		  $this->db->trans_rollback();
		  return false;
	   }
	   else
	   {
		  if($this->db->trans_commit())
		  {
			  return true;
		  }
		  else
		  {
			  return false;
		  }
	   }
	}
	public function add_designer($user_name,$f_name,$l_name,$email,$password)
	{
		$date=date('Y-m-d');
		$data=array('admin_user_name'=>$this->db->escape_str($user_name),
				    'admin_user_password'=>$this->db->escape_str(md5($password)),
				    'admin_user_fname'=>$this->db->escape_str($f_name),
				    'admin_user_lname'=>$this->db->escape_str($l_name),
				    'admin_user_email'=>$this->db->escape_str($email),
				    'admin_user_group'=>'4',
				    'admin_user_verified'=>'0',
				    'admin_user_created'=>$this->db->escape_str($date));
	   $this->db->trans_begin();
	   $insert=$this->db->insert('admin_users', $data); 
	   if ($this->db->trans_status() === FALSE)
	   {
		  $this->db->trans_rollback();
		  return false;
	   }
	   else
	   {
		  if($this->db->trans_commit())
		  {
			  return true;
		  }
		  else
		  {
			  return false;
		  }
	   }
	}
	public function is_valid_reset_key($key,$activate_user=true)
	{
		$this -> db -> select('user_id,user_fname,user_lname,user_redirect_url');
		$this -> db -> from('users');
		$this -> db -> where('user_reset_key',$key);
		$query= $this -> db -> get();
		if($query -> num_rows() == 1)
	    {
	    	  if($activate_user==TRUE)
	    	  {
		 	  if($this->enable_user($key))
		      {
			 	return $query->result();
			  }
			  else
			  {
			 	return FALSE;
			  }
		  }
	    	  else
	    	  {
		 	return $query->result();
		  }
	    }
	    else
	    {
		  return false;
	    }
	}
    public function register_other_portal_user($email,$fname,$lname,$gender,$portal,$user_portal_id)
	{
		$date=date('Y-m-d');
		$data=array('user_name'=>$this->db->escape_str($email),
				    'user_fname'=>$this->db->escape_str($fname),
				    'user_lname'=>$this->db->escape_str($lname),
				    'user_email'=>$this->db->escape_str($email),
					'user_gender'=>$this->db->escape_str($gender),
					'user_portal'=>$this->db->escape_str($portal),
					'user_portal_id'=>$this->db->escape_str($user_portal_id),
				    'user_verified'=>'1',
				    'user_created'=>$this->db->escape_str($date));
	   $this->db->trans_begin();
	   $insert=$this->db->insert('users', $data); 
	   if ($this->db->trans_status() === FALSE)
	   {
		  $this->db->trans_rollback();
		  return false;
	   }
	   else
	   {
		  if($this->db->trans_commit())
		  {
			  return true;
		  }
		  else
		  {
			  return false;
		  }
	   }
	}
   public function login($username, $password)
   {
	 $this -> db -> select('*');
	 $this -> db -> from('users');
	 $this -> db -> where('username', $this->db->escape_str($username));
	 $this -> db -> where('password', $this->db->escape_str(md5($password)));
	 $this -> db -> where('verified', 1);
	 //$this -> db -> where('uac', '255');
	 $query = $this -> db -> get();
	 if($query -> num_rows() == 1)
	 {
	   return $query->result();
	 }
	 else
	 {
	   return false;
	 }
   }
   public function portal_login($email)
   {
	 $this -> db -> select('*');
	 $this -> db -> from('users');
	 $this -> db -> where('user_email', $this->db->escape_str($email));
	 //$this -> db -> where('verified', 1);
	 //$this -> db -> where('uac', '255');
	 $query = $this -> db -> get();
	 if($query -> num_rows() == 1)
	 {
	   return $query->result();
	 }
	 else
	 {
	   return false;
	 }
   }
   
   
   /*for goo water by 0007 shan*/
   
   
   	public function otp_login($phone=0 , $supplier_id=0)
   	{
   			
		 $this -> db -> select('*');
		 $this -> db -> from('user');
		 
		 if(!empty($phone))
		 {
		 	$this -> db -> where('user_name', $this->db->escape_str($phone));
		 	$this -> db -> where('user_group', '3');
		 }
		 
		 if(!empty($supplier_id))
		 {
		 	$this -> db -> where('user_id', $this->db->escape_str($supplier_id));
		 	$this -> db -> where('user_group', '2');
		 }
		 
		
		 $this -> db -> where('user_removed', '0');
		 $query = $this -> db -> get();
		 if($query -> num_rows() == 1)
		 {
		   return $query->result();
		 }
		 else
		 {
		   return false;
		 }
   }
   
   
   /*for goo water by 0007 shan*/
   
   
   
   public function insert_portal_id($user_id,$portal_id)
   {
	   $data=array('user_portal_id'=>$portal_id);
	   $this->db->trans_begin();
	   $this->db->where('id',$user_id); 
	   $this->db->update('users',$data);  
	   if ($this->db->trans_status() === FALSE)
	   {
		  $this->db->trans_rollback();
		  return false;
	   }
	   else
	   {
		  if($this->db->trans_commit())
		  {
			  return true;
		  }
		  else
		  {
			  return false;
		  }
	   }
   }
   public function admin_login($username, $password)
   {
	 $this -> db -> select('*');
	 $this -> db -> from('users');
	 $this -> db -> where('username', $this->db->escape_str($username));
	 $this -> db -> where('password', $this->db->escape_str(md5($password)));
	 $this -> db -> where('verified', 1);
	 $this -> db -> where('uac', '777');
	 $query = $this -> db -> get();
	 if($query -> num_rows() == 1)
	 {
	   return $query->result();
	 }
	 else
	 {
	   return false;
	 }
   }
   public function update_admin_session($id,$username)
   {
	   $this -> db -> select('*');
	   $this -> db -> from('users');
	   $this -> db -> where('id', $this->db->escape_str($id));
	   $this -> db -> where('username', $this->db->escape_str($username));
	   $this -> db -> where('uac', '777');
	   $query = $this -> db -> get();
	   if($query -> num_rows() == 1)
	   {
		 return $query->result();
	   }
	   else
	   {
		 return false;
	   }
   }
   public function mstatus_name($id)
   {
	  $this -> db -> select('mstatus_name');
	  $this -> db -> from('mstatus');
	  $this -> db -> where('mstatus_id',$id);
	  $query = $this -> db -> get();
	  if($query -> num_rows() >= 1)
	  {
	    return $query->result();
	  }
	  else
	  {
	    return false;
	  }
   }
   public function get_mstatus($user_id)
   {
	  $this -> db -> select(array('mstatus'));
	  $this -> db -> from('users');
	  $this -> db -> where('id',$user_id);
	  $query = $this -> db -> get();
	  if($query -> num_rows() >= 1)
	  {
	    return $query->result();
	  }
	  else
	  {
	    return false;
	  }
   } 
   public function update_user($fname,$lname,$mobile,$gender,$email,$address,$password,$image,$id)
   {
	   $pwd=array();
	   if($password!='') $pwd=array('user_password'=>$this->db->escape_str(md5($password)));
	   $data=array('user_fname'=>$this->db->escape_str($fname),
				   'user_lname'=>$this->db->escape_str($lname),
				   'user_email'=>$this->db->escape_str($email),
				   'user_mobile'=>$this->db->escape_str($mobile),
				   'user_gender'=>$this->db->escape_str($gender),
				   'user_address'=>$this->db->escape_str($address),
				   'user_photo_url'=>$this->db->escape_str($image)
				   );
	  $update_array=array_merge($data,$pwd);
	  $this->db->where('id',$id);
	  $this->db->update('users', $update_array);
	  if($this->db->affected_rows() == 1)
	   {
		 return true;
	   }
	   else
	   {
		 return false;
	   } 
   }
   public function enable_user($key)
   {
	   $key1=$this->db->escape_str($key);
	   $data=array('user_reset_key'=>'',
				   'user_verified'=>'1');
	   $this->db->where('user_reset_key', $key1);
	   $this->db->update('users', $data); 
	   if($this->db->affected_rows() == 1)
	   {
	   	//echo '52'; die;
		 return true;
	   }
	   else
	   {
		 return false;
	   }
   }
    public function get_user_info($user_id)
	{
		$this -> db -> select('*');
		$this -> db -> from('users');
		$this -> db -> where('id',$user_id);
		$query = $this -> db -> get();
		if($query -> num_rows() == 1)
		{
			return $query->result(); 
		}
		else
		{
			return false;
		}
	}
	public function get_user_info_by_username($username)
	{
		$this -> db -> select('*');
		$this -> db -> from('users');
		$this -> db -> where('username',$username);
		$query = $this -> db -> get();
		if($query -> num_rows() == 1)
		{
			return $query->result(); 
		}
		else
		{
			return false;
		}
	}
    public function get_address($user_id)
    {
		$this -> db -> select(array('address','address2','city','state','country'));
		$this -> db -> from('users');
		$this -> db -> where('id',$user_id);
		$query = $this -> db -> get();
		if($query -> num_rows() == 1)
		{
			return $query->result(); 
		}
		else
		{
			return false;
		}
    }
	public function users_list()
	{
		$this -> db -> select('*');
		$this -> db -> from('users');
		$this -> db -> where('uac', '255');
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	public function user_detail($id)
	{
		$this -> db -> select('*');
		$this -> db -> from('users');
		$this -> db -> where('id', $id);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	public function insert_session($portal='1')
	{
		//insert in db
		$uniqueId = uniqid($this->session->userdata['ip_address'], TRUE);
		$this->session->set_userdata("my_session_id", md5($uniqueId));
		$ip_address=$this->session->userdata['ip_address'];
		$last_activity=$this->session->userdata['last_activity'];
		$user_agent=$this->session->userdata['user_agent'];
		$logged_in=$this->session->userdata['logged_in'];
		$logged_in1=serialize($logged_in);
		$start_time=time();
		$data=array('session_id'=>md5($uniqueId),'ip_address'=>$ip_address,'start_time'=>$start_time,'last_activity'=>$last_activity,'user_agent'=>$user_agent,'user_data'=>$logged_in1,'portal'=>$portal);
		$query=$this->db->insert('sessions', $data);
		if($query==TRUE)
		{
		   return TRUE;   
		}
		else
		{
		   return FALSE;
		}
	}
	public function delete_session()
	{
		$session_id=$this->session->userdata['my_session_id'];
		$data=array('session_id'=>$session_id);
		$this->db->delete('sessions', $data);
	}
	public function insert_key($key,$email)
	{
		$data=array('reset_id'=>$key);
		$this->db->where('user_email',$email);
		$this->db->update('users', $data); 
		if($this->db->affected_rows() == 1)
		 {
		   return true;
		 }
		 else
		 {
		   return false;
		 }
	}
	public function valid_user_key($key)
	{
		$this -> db -> select('id');
		$this -> db -> from('users');
		$this -> db -> where('reset_id', $key);
		$query = $this -> db -> get();
		if($query -> num_rows() == 1)
		{
			return $query -> result();
		}
		else
		{
			return false;
		}
	}
	public function update_password($password,$user_id)
	{
		$data=array('password'=>md5($password),
					'reset_id'=>'');
		$this -> db ->where('id',$user_id);
		$this -> db ->update('user',$data);
		if($this->db->affected_rows() == 1)
		{
		  return true;
		}
		else
		{
		  return false;
		}
	}
	public function profile_photo($user_id)
	{
		$this -> db -> select('user_photo_url,user_portal_id,user_gender');
		$this -> db -> from('user');
		$this -> db -> where('user_id', $user_id);
		$query = $this -> db -> get();
		if($query -> num_rows() == 1)
		{
			return $query -> result();
		}
		else
		{
			return false;
		}
	}
}