<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
Class Common_model extends CI_Model
{
	
	public function log_details($log_id)
	{
	   $this->db->select('a.*');
	   $this->db->from('log as a');
	   $this->db->where('id',$log_id);
	   
	   $query = $this -> db -> get();
	   if($query -> num_rows()==1)
	   {
	   	 return $query->result_array();
		 
		 
	   }
	   else
	   {
		 return false;
	   }
	}

	public function fetch_by_id($table,$id,$select='*')
	{
		$this->db->select($select);
		$this->db->from($table);
		$this->db->where($id);
		$query = $this -> db -> get();
		/*var_dump($this->db->last_query());*/
	    if($query -> num_rows())
	    {
	   	  return $query->result_array();
		 
		 
	    }
	    else
	    {
		 return false;
	    }
	}

	public function fetch_contents($table_name,$data)
	{
		$this->db->select('*');
		$this->db->from($table_name);
		$this->db->where($data);
		$this->db->where($table_name.'_removed','0');
		$query = $this -> db -> get();
		/*var_dump($this-> db ->last_query());*/
	    if($query -> num_rows())
	    {
	    	//var_dump($query->result_array());
	   	  return $query->result_array();
		 
		 
	    }
	    else
	    {
		 return false;
	    }
	}
	
	public function insert_table($table_name,$data)
	{
		$this->db->insert($table_name,$data);
		return $this->db->affected_rows() > 0;
		
	}
	public function insert_multiple($table_name,$data)
	{
		$this->db->insert_batch($table_name,$data);
		return $this->db->affected_rows() > 0;
		
	}
	
	public function delete_table($table_name,$data)
	{
		$this->db->delete($table_name,$data);
		return $this->db->affected_rows() > 0;
		
	}

	public function update_table($table_name,$data,$id)
	{
		$this->db->where($table_name.'_id',$id);
		$this->db->update($table_name,$data);
		
		if($this->db->error())
		{
			return false;
		}
		return true;
		
	}
	
	public function __update_table($table_name,$data,$wheredata)
	{
		$this->db->where($wheredata);
		
		if($this->db->update($table_name,$data))
		{
			return true;
		}
		return false;
		
	}


	public function dublicate_check($table_name,$array)
	{
		$this->db->select('*');
		$this->db->from($table_name);
		$this->db->where($array);
		$this->db->where($table_name.'_removed','0');
		
		$query=$this->db->get();
		
		if($query->num_rows())
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	public function general_update($table='',$data=array(),$where='',$affected_rows=FALSE)
	{
		if(($data!='')&&(count($data)!=0)&&($where!=''))
		{
			$this->db->where($where['field'], $where['constant']);
			$this->db->update($table, $data);
			//echo $this->db->last_query();die();
			if($affected_rows==TRUE)
			{
				return $this->db->affected_rows();
			}
			else
			{
				return TRUE;
			}
		}
		else
		{
			return false;
		}
	}
	
	/**
	* function general_select()
	* @param string $table is the table name
	* @param array $constrains is the array('type'=>'where/or','field_name'=>'name of the field which will be used to check the condition','value'=>'condition value')
	* @param if($constrains=='') it will not go to check the conditions. It will simply select the fields without any conditions
	* @param string $select is the fields have to sleected specifically. if $select=='', then '*' will be the default
	* @return will be the result based on the query generated
	* join table operations can not be performed
	*/
	public function general_select($table='',$constrains='',$select='*',$order_by=FALSE)
	{
		if($table!='')
		{
			$this->db->select($select);
			$this->db->from($table);
			if($constrains!='')
			{
				foreach($constrains as $constrain)
				{
					$this->db->$constrain['type']($constrain['field_name'],$constrain['value']);
				}	
			}
			if(!empty($order_by))
			{
				$this->db->order_by($order_by);
			}
			$query=$this->db->get();
			//echo $this->db->last_query().'<br/>';
			//var_dump($query->result());
			if($query->num_rows()>=1)
			{
				return $query->result();
			}
			else
			{
				//echo $this->db->last_query().'<br/>';
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}
	public function general_delete($table_name,$where)
	{
		$delete_data=$this->db->delete($table_name,$where); 
		return $delete_data;
	}
	
	
	public function general_insert($table='',$data=array())
	{
		if(($data!='')&&(count($data)!=0))
		{
			$this->db->insert($table, $data);
			//echo $this->db->last_query();
			return TRUE;
		}
		else
		{
			return false;
		}
	}
	public function edit_dublicate_check($table_name,$array,$id)
	{
		$this->db->select('*');
		$this->db->from($table_name);
		$this->db->where($array);
		$this->db->where($table_name.'_removed','0');
		$this->db->where($table_name.'_id <> ',$id);
		
		$query=$this->db->get();
		
		if($query->num_rows())
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}

	public function insert_table_lastid($table_name,$data)
	{
		$this->db->insert($table_name,$data);
		return $this->db->insert_id();
		
	}

 
	public function __fetch_contents($table_name,$data,$select='*',$group_by='',$order_by='',$limit='')
	{
		$this->db->select($select);
		$this->db->from($table_name);
		$this->db->where($data);
		$this->db->group_by($group_by);
		if($order_by!='')
		{
			$this->db->order_by($order_by);	
		}
		if($limit!='')
		{
			$this->db->limit($limit);	
		}
		
//		$this->db->where($table_name.'_removed','0');
		$query = $this -> db -> get();
		 //var_dump($this->db->last_query());
		//var_dump($this->db->last_query());
	    if($query -> num_rows())
	    {
	   	  return $query->result_array();
		 
		 
	    }
	    else
	    {
		 return false;
	    }
	}


	public function __dublicate_check($table_name,$array)
	{
		$this->db->select('*');
		$this->db->from($table_name);
		$this->db->where($array);
		/*$this->db->where($table_name.'_removed','0');*/
		
		$query=$this->db->get();
		//var_dump($this->db->last_query());
		/*echo $this->db->last_query();*/
		if($query->num_rows())
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}


	public function __edit_dublicate_check($table_name,$array,$column,$id)
	{
		$this->db->select('*');
		$this->db->from($table_name);
		$this->db->where($array);
		$this->db->where($column." <> ",$id);
		$query=$this->db->get();
		
		if($query->num_rows())
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}


	public function permission_check($url)
	{
		$this->db->select('b.permission_id');
		$this->db->from('page as a');
		$this->db->join('permission as b','b.page_id=a.page_id');
		$this->db->where('page_url',$url);
		$this->db->where('b.user_group',$this->session->userdata['user']['user_group']);
		
		$query = $this -> db -> get();
		/*var_dump($this->db->last_query());
		die();*/
	    if($query -> num_rows() >= 1)
	    {
		  return $query->result_array();
	    }
	    else
	    {
		  return FALSE;
	    }
	}
	
	
	public function add_curr_my_info($remove_old=0,$new_rcd=0)
	{
		$this->db->trans_begin();
				
		if(!empty($remove_old))
		{
			$removed['cmp_profile_is_current']=1;
			$removed['cmp_profile_is_removed']=1;
			$this->db->where('cmp_profile_id >',0);
			$this->db->update('company_profile',$removed);	
		}
		
				
		if($new_rcd)
		{
			$this->db->insert('company_profile',$new_rcd);
		}		
		
		if($this->db->trans_status()==TRUE)
		{
			$this->db->trans_commit();			
			return TRUE;	
		}
		else
		{
			$this->db->roll_back();
			return FALSE;
		}
	}	

	/**
	*  @author BYZ0006
	* here we get data from my_info tbl
	* 
	* 
	* 
	* @return current my info
	*/
	public function retrive_curr_my_info($id=0)
	{
		$this->db->select('b.*,d.city_name,e.state_name,f.country_name');
		$this->db->from('company_profile as b');		
	  	
		$this->db->join('city as d','d.city_id=b.cmp_profile_city_id');   	
		$this->db->join('state as e','e.state_id=b.cmp_profile_state_id'); 				
		$this->db->join('country as f','f.country_id=b.cmp_profile_country_id');   					
		
		$this->db->where('b.cmp_profile_is_removed',0);
				
		if(!empty($id))
		{
			$this->db->where('b.cmp_profile_id',$id);
		}
		
		$query = $this -> db -> get();	
		
		/*var_dump($this->db->last_query());*/
			
	   if($query -> num_rows())
	   {
	   	return $query->result_array();	   
	   }
	   else
	   {
		 return false;
	   }
	}


	public function get_user_with_group($id=0)
	{
		$this->db->select('a.*,b.usergroup_name');
		$this->db->from('user as a');		
	  	
		$this->db->join('usergroup as b','b.usergroup_id=a.user_group');   	
						
		$this->db->where('a.user_removed',0);
		$this->db->where('a.user_group <>',1);
				
		if(!empty($id))
		{
			$this->db->where('a.user_id',$id);
		}
		
		$query = $this -> db -> get();	
		
		/*var_dump($this->db->last_query());*/
			
	   if($query -> num_rows())
	   {
	   	return $query->result_array();	   
	   }
	   else
	   {
		 return false;
	   }
	}

}