<?php
class Login_model extends CI_Model
{
	public function process_login($email,$password)
	{
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('user_email',$email);
		$this->db->where('user_password',MD5($password));
		$query=$this->db->get();
		//echo $this->db->last_query();
		if($query->num_rows()=='1')
		{
			return $query->result();
		}
		else
		{
			return FALSE;
		}
	}
	public function process_admin_login($username,$password)
	{
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('user_name',$username);
		$this->db->where('user_password',MD5($password));
		$query=$this->db->get();
		//echo $this->db->last_query();die;
		if($query->num_rows()=='1')
		{
			return $query->result();
		}
		else
		{
			return FALSE;
		}
	}
	public function update_session_data_db($data)
	{
		$current_session_id=session_id();
		$this->db->where('id',$current_session_id);
		$this->db->update('ci_session',$data);
		if ($this->db->trans_status() === FALSE)
		{
		  $this->db->trans_rollback();
		  return false;
		}
		else
		{
		  if($this->db->trans_commit())
		  {
			  return TRUE;
		  }
		  else
		  {
			  return false;
		  }
		}
	}
	public function update_session_time()
	{
		$this->db->where('session_id',time());
		$this->db->update('sessions',$data);
		if ($this->db->trans_status() === FALSE)
		{
		  $this->db->trans_rollback();
		  return false;
		}
		else
		{
		  if($this->db->trans_commit())
		  {
			  return TRUE;
		  }
		  else
		  {
			  return false;
		  }
		}
	}
	public function delete_session()
	{
		$session_id=$this->session->userdata['my_session_id'];
		$data=array('session_id'=>$session_id);
		$this->db->delete('logged_users', $data);
	}
}