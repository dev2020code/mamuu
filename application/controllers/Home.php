<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	 public function __construct()
	 {
	 	parent::__construct();
	 }
	 public function index()
	 {

		$this->load->view('master/home-view');

	 }

	 public function add_start_process()
	 {
	   if ($this->input->is_ajax_request())
	   {
			$data=$this->input->post();
			// var_dump($_FILES['empFile']['name']);
			// die;
			$this->load->library('form_validation');
			$this->form_validation->set_rules('fname','Name','required');			
			$this->form_validation->set_rules('phone','Contact','min_length[10]|required');
			$this->form_validation->set_rules('email','Email','required|valid_email');
			
			$this->form_validation->set_rules('forex','forex','required');			
		    if($this->form_validation->run()!==false)
		   	{
		   		$starter['strName']=$data['fname'];
		   		$starter['strPhone']=$data['phone'];
		   		$starter['strEmail']=$data['email'];
				$emp_id=$this->common_model->insert_table('starter',$starter);
			    if($emp_id!==false)
			    {
			        $subject = 'Registration From Mammu Export.net';
                	$text    = 'Name :'.$data['fname'].'<br> Phone Number :'.$data['phone'].'<br> Email :'.$data['email'].'<br> Trader Type :'.$data['forex'];
                	$to 	= admin_mail;
		                $this->load->library('email_lib');
		                $this->email_lib->normal_mail($to, $subject, $text);
		                // $textuser    = 'Name :'.$data['fname'].'<br> Phone Number :'.$data['phone'].'<br> Email :'.$data['email'].'<br>Thanks for registering with us';
		                // $touser	=$data['email'];
		                $touser='support@mamuuforex.com';
		                $this->email_lib->normal_mail($touser, $subject, $text);
		                $touser1='info@mamuuexpogroup.com';
		                $this->email_lib->normal_mail($touser1, $subject, $text);
		                $touser2='mamuuexpo@gmail.com';
		                $this->email_lib->normal_mail($touser2, $subject, $text);

			    	$message='Thanks for your interest. Our technical executive will contact you shortly';
			        $report = array('status' => 1,'message' => $message);
			        echo json_encode($report);
					exit;
			    }
			    else
			    {
			        $message = 'Something wrong please try again';
			        $report  = array('status' => 0,'message' => $message);
			        echo json_encode($report);
			        exit;

			    }
		   	}
		   	else
		   	{
		     $message = $this->form_validation->error_array();
		     $report  = array('status' => 0,'message' => $message);
		     echo json_encode($report);
		     exit;
		    }
	    }
	    else
	   {
	     show_error("No direct access allowed");
	     //or redirect to wherever you would like
	   }
	}
	function upload_file($filename='')
    {
    	
        // $config['upload_path']   = $_SERVER['DOCUMENT_ROOT'];
        $config['allowed_types'] = 'doc|docx|pdf|gif|jpg|png|jpeg|svg';
        $new_name                = time();
        $config['file_name']     = $new_name . '_' . 'resume';
        $config['upload_path']   = site_path.'assets/phoenix';
        $config['max_size']      = 10000;
        if ($filename=='emplrFile') {
        	$config['file_name']     = $new_name . '_' . 'Company Profile';
        }

        $this->load->library('upload', $config);
     //    var_dump($filename);
    	// die;
        if ($this->upload->do_upload($filename)) {
            return array('success'=>$this->upload->data());
        } else {
            return array('failure'=>$this->upload->display_errors());
        }
    }
 }
 ?>
