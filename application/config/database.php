<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$active_group = 'default';
$query_builder = TRUE;
if($_SERVER['SERVER_NAME']=='localhost' || $_SERVER['SERVER_NAME']=='192.168.1.2' )
{
	$username = "root";
	$password = "";
	$database = "mamuu";
}
else
{
	$username = "wwwgetln_sample";
	$password = "Byzero#sample_user";
	$database = "wwwgetln_mamuu";
}
$db['default'] = array(
	'dsn'	=> '',

	'hostname' => 'localhost',
	'username' => $username,
	'password' => $password,
	'database' => $database,
	'dbdriver' => 'mysqli',
	'dbprefix' => 'mam_',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);
