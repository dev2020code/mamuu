<?php defined('BASEPATH') OR exit('No direct script access allowed');
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');


define('no_res_found','No Record Found');



if($_SERVER['SERVER_NAME']=='localhost' || $_SERVER['SERVER_NAME']=='192.168.1.100')
{
	define('site_url', 'http://'.$_SERVER['SERVER_NAME'].'/projects/mamuu/');
	define('DS',"/");//DS is directory seperator
	define('site_path',$_SERVER['DOCUMENT_ROOT'].'/projects/mamuu/');

}
else
{
	define('site_url', 'http://'.$_SERVER['SERVER_NAME'].'/mamuu/06/');
	define('DS',"/");//DS is directory seperator
	define('site_path',$_SERVER['DOCUMENT_ROOT'].'/mamuu/06/');

}
define('admin_folder','admin');
define('admin_url', site_url.admin_folder.'/');
define('assets_url',site_url.'assets/');
define('images_url',site_url.'assets/images/');
define('css_url',site_url.'assets/css/');
define('js_url',site_url.'assets/js/');

define('admin_assets_url',site_url.'assets/admin_assets/');


define('admin_css_url',admin_assets_url.'css/');
define('admin_js_url',admin_assets_url.'js/');
define('user_profile_photo_url',admin_assets_url.'logo_upload/');
define('site_title','Nandha Engineering Complaint Portal');
define('title_tag',' - Nandha Engineering Complaint Portal');
//define('admin_mail','phoenixhrplacement@gmail.com');
//define('admin_mail','admin@mamuuforex.com');
define('admin_mail','prsd1991@gmail.com');



date_default_timezone_set('Asia/Kolkata');

defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code
