<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$autoload['packages'] = array();
$autoload['libraries'] = array('database','session','Validate_login','users_lib');
$autoload['drivers'] = array();
$autoload['helper'] = array('url','security','goowater');
$autoload['config'] = array();
$autoload['language'] = array();
$autoload['model'] = array('common_model','user_model','data_tbl_retrive_data');
